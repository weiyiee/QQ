# Future QQ

为了应对Linux平台下原生QQ不好用的情况，特此开发基于安卓QQ协议的跨平台客户端。

能够在Linux（主要），Windows，Mac，Android平台运行。

## 已经实现的功能

- 密码登录
- 群列表
- 好友列表
- 群消息
- 好友消息
- 断线重连

## 运行截图&演示

[点我看演示视频](https://www.bilibili.com/video/BV167411P78k/)

记得点赞投币关注三连啊！

![登录界面](doc/img/a.png)

![好友列表](doc/img/b.png)

![群列表](doc/img/c.png)

![消息列表](doc/img/d.png)

![可以隐藏聊天框](doc/img/f.png)

![在Linux下的实际显示效果，包括托盘图标](doc/img/e.png)

## 编译&开发

需要以下条件（旧版Protobuf会有问题）

- 任何Linux发行版
- 手
- QT
- QT Creator（可选）
- Google Protobuf 3.0以上
- Zlib
- OpenSSL（可选）

使用QT打开项目直接编译运行即可，项目依赖都给你无脑写好了。

没有QT Creator？试试

```bash
mkdir build && cd build && qmake .. && make
```

## 联系方式

QQ群：221359737

QQ号码：2836365231

在校学生，代码不易，给个Star？

有没有大公司给口饭吃？嘤嘤嘤。

## 好像还少点什么？

可以看看我的另一个项目

[QQ.NET](https://gitee.com/Finch1/QQ.NET)

以及我的更多开源项目。

## 开源须知

严禁利用本项目做违法违德的事情（包括但不限于黑产等。）

修改或基于本项目的一切硬件和软件都必须开源。

## 免责声明

本项目仅供学习交流使用，因本项目造成一切法律纠纷与原作者无关。

使用时请遵守腾讯相关协议与相关法律法规。


#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QIcon>
#include "../AndroidQQ/androidqqcore.h"
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    AndroidQQCore::registerClass();//DONT FORGET THIS
    QApplication app(argc, argv);
    QIcon ico = QIcon("qrc:/ui/assets/qq.ico");
    app.setWindowIcon(ico);
    QQmlApplicationEngine engine;
    AndroidQQCore core;
    engine.rootContext()->setContextProperty("client",&core);
    const QUrl url(QStringLiteral("qrc:/ui/Main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
    return app.exec();
}

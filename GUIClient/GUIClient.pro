TEMPLATE = app
TARGET = QQ
QT += quick widgets
CONFIG += c++11

RC_ICONS = ui/assets/qq.ico

RESOURCES += \
    qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

SOURCES += main.cpp

unix:!macx: LIBS += -L$$OUT_PWD/../AndroidQQ/ -lAndroidQQ

INCLUDEPATH += $$PWD/../AndroidQQ
DEPENDPATH += $$PWD/../AndroidQQ

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../AndroidQQ/libAndroidQQ.a

include(../AndroidQQ/AndroidQQ.pri)

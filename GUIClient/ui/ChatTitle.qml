import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
Rectangle{
    id:chattitle
    color: "lightgrey"
    Row{
        spacing: 10
        CircularImage{
            id: headImg
            img_src: ""
            x: 5
            y: 5
            width: chattitle.height - 10
            height: chattitle.height - 10
        }
        Column{
            anchors.verticalCenter: parent.verticalCenter
            Text {
                id: remark_text
                text: "Finch"
                font.pixelSize: chattitle.height * 0.4
            }
            Text {
                id: uin_text
                text: "123456789"
                font.pixelSize: chattitle.height * 0.3
                color: "grey"
            }
        }
    }
    function setTitle(uin,remark,description,head){
        headImg.img_src = head
        remark_text.text = remark
        uin_text.text = uin
    }
}

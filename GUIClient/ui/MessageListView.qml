import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item{
    enum MessageType{
        Friend,Group
    }

    property var onListItemSelected: null
    property var selectedModel: null
    property var msgListMap: ({})
    //width: 300
    //height: 700
    TextField{
        id:filterInput
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        font.pixelSize: 16
        placeholderText: "搜索..."
        Keys.onPressed:{
            switch(event.key){
            case Qt.Key_Return:
            case Qt.Key_Enter:
                break
            }
        }
    }
    ListView{
        id: listview
        clip: true
        width: parent.width
        anchors.top: filterInput.bottom
        anchors.bottom: parent.bottom
        highlightFollowsCurrentItem: true
        highlight: Rectangle{
            width: parent.width
            height: 60
            color: "lightgrey"
            radius: 5
        }
        model: ListModel{
            id:msgList
            dynamicRoles: true
        }
        delegate: Item {
            height: 60
            width: parent.width
            Row{
                x: 10
                y: 5
                height: parent.height
                spacing: 10
                CircularImage{
                    height: parent.height - 10
                    width: parent.height - 10
                    img_src: model.head
                }
                Column{
                    Text {
                        font.pixelSize: 18
                        text: model.name
                        //text: "隐藏备注（隐藏昵称）"
                    }
                    Text {
                        text: model.uin
                        //text: "123456789"
                        color: "grey"
                    }
                }
            }
            MouseArea{
                width: listview.width
                height: parent.height
                onClicked: {
                    listview.currentIndex = index
                    selectedModel = model
                    onListItemSelected(model)
                }
            }
        }
    }
    Component{
        id:templateModel
        ListModel{
            dynamicRoles: true
        }
    }

    function append(messageType,name,head,uin,msgBody){
        if(msgListMap[uin] === undefined){
            msgListMap[uin] = {
                messageType:messageType,
                name:name,
                head:head,
                uin:uin,
                msgBody:templateModel.createObject()//amazing it's a pointer
              }
            msgListMap[uin].msgBody.append(msgBody)
            msgList.append({
                               messageType:messageType,
                               name:name,
                               head:head,
                               uin:uin,
                               msgBody:msgListMap[uin].msgBody
                             })
        }else{
            msgListMap[uin].msgBody.append(msgBody)
            //TODO CLEAR SO MANY MESSAGE
        }
        //so make the msgbody a list for listmodel
    }
    function selectUin(uin){
        //no implement
    }
}

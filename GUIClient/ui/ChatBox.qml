import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    //property var msgModel: null
    //color: "#e0e0e0"
    ListView {
        id: listView_msg
        clip: true
        anchors.fill: parent
        anchors.topMargin: 10
        anchors.bottomMargin: 10
        spacing: 10
        keyNavigationWraps: true
        focus: true

        delegate: Column {
            id: column
            anchors.right: isMe ? parent.right : undefined
            anchors.rightMargin: isMe ? 20 : undefined
            anchors.left: isMe ? undefined : parent.left
            anchors.leftMargin: isMe ? undefined : 20
            spacing: 6

            Label {
                visible: true
                id: timestampText
                text: nick
                color: "grey"
                anchors.right: isMe ? parent.right : undefined
            }

            Row {
                id: messageRow
                spacing: 6
                anchors.right: isMe ? parent.right : undefined
                layoutDirection: isMe ? Qt.RightToLeft : Qt.LeftToRight
                CircularImage {
                    id: avatar
                    width: 35
                    height: 35
                    //fillMode: Image.PreserveAspectFit
                    //source: model.head
                    img_src: model.head
                }

                Rectangle {
                    id: contentBox
                    width: Math.min(
                               (content!==""?(messageText.implicitWidth)+24:(messageImage.width)),
                               0.6 * (listView_msg.width - avatar.width - messageRow.spacing))
                    height: (content!==""?(messageText.implicitHeight)+24:(messageImage.height))
                    color: isMe ? "lightgrey" : "steelblue"
                    radius: 10
                    Label {
                        id: messageText
                        text: content
                        color: isMe ? "black" : "white"
                        anchors.margins: 12
                        anchors.fill: parent
                        wrapMode: Label.Wrap
                        Component.onCompleted: {
                            if(content === "")visible = false
                        }
                    }
                    Image {
                        id: messageImage
                        source: ""
                        anchors.left: parent.left
                        anchors.leftMargin: 0
                        anchors.top: parent.top
                        anchors.topMargin: 0
                        MouseArea{
                            anchors.fill: parent
                            onClicked: {
                                //show the raw image
                            }
                        }
                        Component.onCompleted: {
                            if(image === "" || content !== ""){
                                visible = false
                                return
                            }
                            contentBox.color = "#ffffff"
                            source = image
                            var maxWidth = 0.6 * (listView_msg.width - avatar.width - messageRow.spacing)
                            if(imgWidth > maxWidth){
                                imgHeight = imgHeight * maxWidth / imgWidth
                                imgWidth = maxWidth
                            }
                            width = imgWidth
                            height = imgHeight
                        }
                    }
                }
            }
        }
    }
    function setModel(model){
        listView_msg.model = model
        listView_msg.positionViewAtEnd()
    }
}

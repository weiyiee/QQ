import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Column{
    property var sendOnClick: null
    property var chatBox: chatBox

    ChatBox{
        width: parent.width
        height: parent.height - 48
        id:chatBox
    }

    Row {
        id: element
        width: parent.width
        height: 48
        spacing: 5
        x: 10

        TextField {
            id: textField
            placeholderText: "请输入消息..."
            height: parent.height
            width: parent.width-(sendbutton.implicitHeight)*2-10
            font.pixelSize: 16
            Keys.onPressed:{
                switch(event.key){
                case Qt.Key_Return:
                    sendbutton.onClicked()
                    break
                case Qt.Key_Enter:
                    sendbutton.onClicked()
                    break
                }
            }
        }

        RoundButton {
            id: sendbutton
            width: parent.height
            height: parent.height
            text: ">"
            font.pixelSize: 24
            anchors.verticalCenter: parent.verticalCenter
            onClicked: {
                sendOnClick(textField.text)
                textField.text = ""
            }
        }

        RoundButton {
            id: imgbutton
            width: parent.height
            height: parent.height
            text: "+"
            font.pixelSize: 24
            anchors.verticalCenter: parent.verticalCenter
        }
    }
}

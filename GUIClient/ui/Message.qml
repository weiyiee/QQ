import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls 1.4
import AndroidQQCore 1.0
import QtQuick.Controls.Material 2.12

Page {
    id: page
    height: 600
    width: 800

    enum MessageType{
        Friend,Group,MessageList
    }

    //Material.accent: Material.Green
    Material.theme: Material.Light

    property var friendList: null
    property var groupList: null
    property var uinToIndex: ({})
    property var groupCodeToIndex: ({})
    property var messageType: null
    property var friendMessageList: ({})

    Component.onCompleted: {

    }

    Connections{
        target: client
        onMessage: function(data){
            switch(data["type"]){
            case AndroidQQCore.StatSvcReg:
                client.getFriendList()
                client.getGroupList()
                break
            case AndroidQQCore.DisConnectedServer:
                client.connnectToHost()
                break
            case AndroidQQCore.ForceOffline:
                //statusOutput.text = "您已被强制下线"
                switchToPage(Main.PageEnum.Login)
                break
            case AndroidQQCore.FriendList:
                friendList = data["friends"]["7"]
                for(var index in friendList){
                    var friend = friendList[index]
                    uinToIndex[friend["0"]] = index
                    friendListView.append(friend["14"],friend["0"],getHeadUrl(friend["0"]),friend["3"])
                }
                client.getMessage()
                break
            case AndroidQQCore.GetTroopListReqV2:
                groupList = data["group"]["5"]
                for(index in groupList){
                    var group = groupList[index]
                    groupCodeToIndex[group["1"]] = index
                    groupListView.append(group["0"],group["1"],group["4"],group["5"],group["23"],getGroupHeadUrl(group["1"]))
                }
                break
            case AndroidQQCore.GroupMessgae:
                messageListView.append(MessageListView.MessageType.Group,data["groupName"],getGroupHeadUrl(data["groupCode"]),data["groupCode"],
                                       buildMessageBody(data["Nick"],data["message"],getChatImgUrl(data["img"]),data["height"],data["width"],getHeadUrl(data["Uin"]),data["Uin"]==uin))
                break
            case AndroidQQCore.FriendMessage:
                for(index in data["content"]){
                    var msg = data["content"][index]
                    messageListView.append(MessageListView.MessageType.Friend,friendList[uinToIndex[msg["Uin"]]]["3"],getHeadUrl(msg["img"]),msg["Uin"],
                                           buildMessageBody(friendList[uinToIndex[msg["Uin"]]]["3"],msg["message"],getChatImgUrl(msg["img"]),msg["height"],msg["width"],getHeadUrl(msg["Uin"]),msg["Uin"]==uin))
                }
                break
            }
        }
    }

    ChatTitle{
        id: chattitle
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        height: 64
        Component.onCompleted: {
            setTitle(uin,nick,"",getHeadUrl(uin))
        }
    }

    SplitView{
        orientation: Qt.Horizontal
        anchors.top: chattitle.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        SwipeView {
            id: swipeView
            x: 0
            width: 280
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
            clip: true

            FriendListView{
                id: friendListView
                onListItemSelected: function(model){
                    chattitle.setTitle(model.uin,model.remark,"",getHeadUrl(model.uin))
                    messageType = Message.MessageType.Friend
                }
            }
            GroupListView{
                id: groupListView
                onListItemSelected: function(model){
                    chattitle.setTitle(model.code,model.name,"",getGroupHeadUrl(model.code))
                    messageType = Message.MessageType.Group
                }
            }
            MessageListView{
                id: messageListView
                onListItemSelected: function(model){
                    chatArea.chatBox.setModel(model.msgBody)
                    messageType = Message.MessageType.MessageList
                    var head = ""
                    if(model.messageType === MessageListView.MessageType.Group)head = getGroupHeadUrl(model.uin)
                    else if(model.messageType === MessageListView.MessageType.Friend)head = getHeadUrl(model.uin)
                    chattitle.setTitle(model.uin,model.name,"",head)
                }
            }
        }

        ChatArea{
            id: chatArea
            anchors.left: swipeView.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            sendOnClick: function(text){
                switch(messageType){
                case Message.MessageType.Friend:
                    client.sendMessageToFriend(friendListView.selectedModel.uin,text)
                    break
                case Message.MessageType.Group:
                    client.sendMessageToGroup(groupListView.selectedModel.code,text)

                    break
                case Message.MessageType.MessageList:
                    if(messageListView.selectedModel.messageType === MessageListView.MessageType.Group)
                        client.sendMessageToGroup(messageListView.selectedModel.uin,text)
                    else if(messageListView.selectedModel.messageType === MessageListView.MessageType.Friend)
                        client.sendMessageToFriend(messageListView.selectedModel.uin,text)
                    break
                }
            }
        }
    }

    function buildMessageBody(nick,content,image,imgHeight,imgWidth,head,isMe) {
        return {
              nick:nick,
              content:content,
              image:image,
              imgHeight:imgHeight,
              imgWidth:imgWidth,
              head:head,
              isMe:isMe
          }
    }
}

/*##^##
Designer {
    D{i:1;anchors_height:200;anchors_width:200;anchors_x:388;anchors_y:143}D{i:2;anchors_height:487;anchors_y:72}
D{i:4;anchors_x:271;anchors_y:497}D{i:3;anchors_height:200;anchors_width:200;anchors_x:388;anchors_y:143}
D{i:7;anchors_width:427;anchors_x:109;anchors_y:12}D{i:9;anchors_width:427;anchors_x:109;anchors_y:12}
D{i:10;anchors_width:427;anchors_x:109;anchors_y:12}D{i:11;anchors_x:0;anchors_y:560}
}
##^##*/

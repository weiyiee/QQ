import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import AndroidQQCore 1.0
import Qt.labs.platform 1.1

ApplicationWindow {
    id: mainWindow
    visible: true
    height: 600
    width: 350
    title: qsTr("QQ")
    onClosing: {
        visible = false
        close.accepted = false
    }

    enum PageEnum{
        Login,Message
    }

    property Loader loader: loader
    property ApplicationWindow mainWindow: mainWindow
    property var switchToPage: function(page){
        switch(page){
        case Main.PageEnum.Login:
            mainWindow.height = 600
            mainWindow.width = 350
            loader.source = "Login.qml"
            break
        case Main.PageEnum.Message:
            mainWindow.height = 700
            mainWindow.width = 1000
            loader.source = "Message.qml"
            break
        }
    }
    property var getHeadUrl: function (uin){
        if(uin === "")return ""
        //return "http://qlogo3.store.qq.com/qzone/"+uin+"/"+uin+"/50";
        return "http://q1.qlogo.cn/g?b=qq&nk="+uin+"&s=640"
        //140   q4
    }
    property var getChatImgUrl: function(originUrl){
        if(originUrl === "")return ""
        return "http://gchat.qpic.cn"+originUrl
    }
    property var getGroupHeadUrl: function(code){
        if(code === "")return ""
        //return "http://121.51.0.105/gh/"+code+"/"+code+"_1/0"
        return "http://p.qlogo.cn/gh/"+code+"/"+code+"_1/140"
    }

    property var nick: ""
    property var uin: 0

    Component.onCompleted: {
        switchToPage(Main.PageEnum.Login)
        //loader.source = "FriendListView.qml"
    }

    Loader{
        asynchronous: false
        id:loader
        //source: "Login.qml"
        anchors.fill: parent
    }

    SystemTrayIcon{
        id: tray
        visible: true
        icon.source: "qrc:/ui/assets/logo.png"
        //icon.name: "Hello world"
        //tooltip: "Fuck your mom"
        menu: Menu {
            MenuItem {
                text: qsTr("退出")
                onTriggered: Qt.quit()
            }
        }
        Component.onCompleted: {
            //showMessage("title","message",3000)
        }
        onActivated: {
            mainWindow.visible = true
        }
    }
}

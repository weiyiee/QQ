import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import AndroidQQCore 1.0

Page {
    id: page
    width: 350
    height: 600

    Material.theme: Material.Light
    //Material.accent: Material.Green

    Connections{
        target: client
        onMessage: function(data){
            switch(data["type"]){
            case AndroidQQCore.ConnectedServer:
                loginButton.enabled = true
                break
            case AndroidQQCore.Login:
                if(data["LoginResponse"] === 0){
                    //login success
                    uin = idInput.text
                    nick = data["nick"]
                    statusText.text = "登录成功！欢迎您："+nick
                    //delayTimer.start()
                    //mainWindow.height = 600
                    //mainWindow.width = 800
                    //loader.source = "Message.qml"
                    switchToPage(Main.PageEnum.Message)
                }
                busyIndicator.running = false
                busyIndicator.visible = false
                break
            }
        }
    }

    Component.onCompleted: {
        client.connnectToHost()
    }

    Timer{
        id:delayTimer
        interval: 2000
        onTriggered: {
            //load page
            mainWindow.height = 600
            mainWindow.width = 800
            loader.source = "Message.qml"
            delayTimer.stop()
        }
    }

    TextField {
        id: idInput
        y: 155
        width: parent.width * 0.7
        height: 70
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        placeholderText: qsTr("QQ号码")
        font.pixelSize: height * 0.5
    }

    TextField {
        id: passInput
        y: 234
        width: parent.width * 0.7
        height: 70
        anchors.horizontalCenterOffset: 0
        placeholderText: qsTr("QQ密码")
        font.pixelSize: height * 0.5
        anchors.horizontalCenter: parent.horizontalCenter
        echoMode: "Password"
        Keys.onPressed:{
            switch(event.key){
            case Qt.Key_Return:
                loginButton.onClicked()
                break
            case Qt.Key_Enter:
                loginButton.onClicked()
                break
            }
        }
    }

    Button{
        id:loginButton
        y: 342
        width: 140
        text: qsTr("登录")
        enabled: false
        font.pixelSize: height * 0.6
        height: 50
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        onClicked: {
            client.login(idInput.text,passInput.text)
            busyIndicator.running = true
            busyIndicator.visible = true
            statusText.text = "登录中..."
        }
    }

    BusyIndicator {
        id: busyIndicator
        x: 145
        y: 420
        visible: false
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Image {
        id: logoImg
        x: 26
        y: 23
        width: 90
        height: 50
        fillMode: Image.PreserveAspectFit
        source: "assets/logo.png"
    }

    Label {
        id: statusText
        x: 32
        y: 534
        width: 287
        height: 43
        text: qsTr("Welcome,Sir.")
        anchors.horizontalCenter: parent.horizontalCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 26
    }
}

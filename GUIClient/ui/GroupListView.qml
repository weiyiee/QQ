import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item{
    property var onListItemSelected: null
    property var selectedModel: null
    //width: 300
    //height: 700
    TextField{
        id:filterInput
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        font.pixelSize: 16
        placeholderText: "搜索..."
        Keys.onPressed:{
            switch(event.key){
            case Qt.Key_Return:
            case Qt.Key_Enter:
                break
            }
        }
    }
    ListView{
        id: listview
        clip: true
        width: parent.width
        anchors.top: filterInput.bottom
        anchors.bottom: parent.bottom
        highlightFollowsCurrentItem: true
        highlight: Rectangle{
            width: parent.width
            height: 60
            color: "lightgrey"
            radius: 5
        }
        model: ListModel{
            id:grouplist
            dynamicRoles: true
        }
        delegate: Item {
            height: 60
            width: parent.width
            Row{
                x: 10
                y: 5
                height: parent.height
                spacing: 10
                CircularImage{
                    height: parent.height - 10
                    width: parent.height - 10
                    img_src: model.head
                }
                Column{
                    Text {
                        font.pixelSize: 18
                        text: model.name
                        //text: "隐藏备注（隐藏昵称）"
                    }
                    Text {
                        text: model.code
                        //text: "123456789"
                        color: "grey"
                    }
                }
            }
            MouseArea{
                width: listview.width
                height: parent.height
                onClicked: {
                    listview.currentIndex = index
                    selectedModel = model
                    onListItemSelected(model)
                }
            }
        }
    }
    function append(uin,code,name,memo,owner,head){
        grouplist.append({
                          uin:uin,
                          name:name,
                          code:code,
                          memo:memo,
                          owner:owner,
                          head:head
                          })
    }
}

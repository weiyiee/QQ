﻿#ifndef QQDEVICECONFIG_H
#define QQDEVICECONFIG_H

#include <QObject>
#include <QStringList>
#include "../Utils/util.h"
#include "../generated/DeviceInformation.pb.h"

class QQDeviceConfig : public QObject
{
    Q_OBJECT
public:
    ~QQDeviceConfig();
    static QQDeviceConfig & getInstance();
    long appId = 16L;
    long subAppId = 537062845L;
    int miscBitMap = 184024956;
    int mainSigMap = 16724722;
    int subSigMap = 0x10400;
    int appClientVersion = 0;
    QByteArray apkID = "com.tencent.mobileqq";
    QByteArray osType = "android";
    QByteArray simInfo = "T-Mobile";
    QByteArray incremental = "5891938";
    QByteArray release = "10";
    QByteArray codeName = "REL";
    int sdk = 29;
    QByteArray apn = "wifi";
    QByteArray imsiMd5 = Util::md5(Util::getRandomByteArray(16));
    QByteArray imei = Util::getRandomNumString(15);
    QByteArray androidId = "Mulan.200122.001"; // display
    QByteArray macAddress = "02:00:00:00:00:00";
    QByteArray wifiBSSID = macAddress;
    QByteArray wifiSSID = "<unknown ssid>";
    QByteArray display = "Mulan.200122.001";
    QByteArray product = "Mulan";
    QByteArray device = "Mulan";
    QByteArray board = "Mulan";
    QByteArray brand = "Finch";
    QByteArray model = "Mulan";
    QByteArray bootloader = "unknown";
    QByteArray fingerprint = "Finch/Mulan/Mulan:10/Mulan.200122.001/5891938:user/release-keys";
    QByteArray procVersion = "Linux version 3.0.31-g6fb96c9 (android-build@xxx.xxx.xxx.xxx.com)";
    QByteArray guid = getGUID();
    QByteArray tgtgKey = getTgtgKey();
    QByteArray randomKey = Util::getRandomByteArray(16);
    QByteArray localIP = QByteArray::fromHex("7f 00 00 01");//127.0.0.1
    QByteArray ssoIP = QByteArray::fromHex("00 01 00 00 00 00 00 7F");
    QByteArray bootID = Util::getUUID().toLatin1();
    short networkType = 2;
    long openAppID = 715019303L;
    QByteArray apkVersionName = "8.2.0";
    QByteArray buildVer = "8.2.0.1296";
    QByteArray apkSignatureMd5 = QByteArray::fromHex("A6 B7 45 BF 24 A2 C2 77 52 77 16 F6 F3 6E B6 8D");
    QStringList domains = {
        "tenpay.com",
        "openmobile.qq.com",
        "docs.qq.com",
        "connect.qq.com",
        "qzone.qq.com",
        "vip.qq.com",
        "qun.qq.com",
        "game.qq.com",
        "qqweb.qq.com",
        "office.qq.com",
        "ti.qq.com",
        "mail.qq.com",
        "qzone.com",
        "mma.qq.com"
    };
    QByteArray ksid = "|454001228437590|A8.2.0.27f6ea96";
    QByteArray baseBand = "";
    short protocolVersion = 8001;
    QString VendorName = "MIUI";
    QString VendorOSName = "?ONEPLUS A5000_23_17";
    qqprotobuf::DeviceInformation deviceData;
signals:
private:
    QQDeviceConfig();
    QByteArray getGUID();
    QByteArray getTgtgKey();
private:
    static QQDeviceConfig * instance;
};

#endif // QQDEVICECONFIG_H

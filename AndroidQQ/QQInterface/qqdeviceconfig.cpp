#include "qqdeviceconfig.h"

QQDeviceConfig * QQDeviceConfig::instance = nullptr;

QQDeviceConfig::~QQDeviceConfig()
{
    if(instance != nullptr)delete instance;
}

QQDeviceConfig &QQDeviceConfig::getInstance()
{
    if(instance == nullptr){
        instance = new QQDeviceConfig();
        //instance->guid = instance->getGUID();
        //instance->tgtgKey = instance->getTgtgKey();
    }
    return *instance;
}

QQDeviceConfig::QQDeviceConfig()
{
    deviceData.set_bootloader(bootloader);
    deviceData.set_procversion(procVersion);
    deviceData.set_codename(codeName);
    deviceData.set_incremental(incremental);
    deviceData.set_fingerprint(fingerprint);
    deviceData.set_bootid(bootID);
    deviceData.set_androidid(androidId);
    deviceData.set_baseband(baseBand);
    deviceData.set_innerversion(incremental);
}

QByteArray QQDeviceConfig::getGUID()
{
    return Util::md5(androidId+macAddress);
}

QByteArray QQDeviceConfig::getTgtgKey()
{
    if(guid.size() == 0)abort();//the size is wrong
    //maybe the value is not init
    return Util::md5(Util::getRandomByteArray(16)+guid);
}

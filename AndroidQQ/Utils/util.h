#ifndef UTIL_H
#define UTIL_H

#include <QObject>
#include <QByteArray>
#include <QCryptographicHash>
#include <QUuid>
#include <QDateTime>
#include <QDataStream>

class Util : public QObject
{
    Q_OBJECT
public:
    static QByteArray str2Hex(const QString & data);//convert "06 5A 4B" >> binary.it can auto cut the space
    static QByteArray md5(const QByteArray & data);//it's length is 16 fixed
    static QByteArray getRandomByteArray(int length);
    static QByteArray cutBytes(const QByteArray & data,int limitLength);//the limitLength must <= data.size()
    static QByteArray getRandomNumString(int length);//get a string like "132451515564"
    static QString getUUID();//04D5E631-096D-995B-26C0-5EAC92BFB48B
    static int getRandomNumber();//get a random number
    static uint getUTCTime();//get the time stamp
    template<typename T>
    static QByteArray getBytes(const T & data,bool bigEndian = true);
    template<typename T>
    static T getData(const QByteArray & data,bool bigEndian = true);
    static std::string BytesStringToStd(const QByteArray & hexStr);
signals:

};

template<typename T>
QByteArray Util::getBytes(const T &data,bool bigEndian)
{
    QByteArray ret;
    QDataStream ds(&ret,QIODevice::ReadWrite);
    ds.setByteOrder(bigEndian?QDataStream::BigEndian:QDataStream::LittleEndian);
    ds<<data;
    return ret;
}

template<typename T>
T Util::getData(const QByteArray & data,bool bigEndian){
    T ret;
    QDataStream ds(data);
    ds.setByteOrder(bigEndian?QDataStream::BigEndian:QDataStream::LittleEndian);
    ds>>ret;
    return ret;
}

//the code is just a big shit!!!!!!
//sha bi c plus plus!!!!

#endif // UTIL_H

#ifndef TEA_H
#define TEA_H

#include <QObject>
#include <QByteArray>

class TEA : public QObject
{
    Q_OBJECT
public:
    static void setKey(const QByteArray & key);//the key length must more than 16
    static void setKey(const long key[4]);
    static QByteArray encode(const QByteArray & data);
    static QByteArray decode(const QByteArray & data,const uint offset = 0);
    static void encodeOneChunk();//encode 8 bytes
    static bool decodeOneChunk(const QByteArray & data,const uint offset,const uint size);//decode 8 bytes
    static QByteArray encrypt(const QByteArray & data,const uint offset,const uint size);
    static QByteArray encrypt(const QByteArray & data);
    static QByteArray encrypt(const QByteArray & data,const QByteArray & key);
    static QByteArray decrypt(const QByteArray & data,const uint offset,const uint size);
    static QByteArray decrypt(const QByteArray & data);
    static QByteArray decrypt(const QByteArray & data,const QByteArray & key);
    static long unpack(const QByteArray & data,uint offset,const uint size = 4);
    static QByteArray twoIntToBytes(int a,int b);
signals:

private:
    static long mKey[4];
    static QByteArray plain;
    static QByteArray prePlain;
    static QByteArray output;
    static int indexCrypt;
    static int preIndexCrypt;
    static int pos;
    static int padding;
    static bool header;
    static int contextStart;
};

#endif // TEA_H

#include "util.h"

QByteArray Util::str2Hex(const QString & data)
{
    return QByteArray::fromHex(data.toLatin1());
}

QByteArray Util::md5(const QByteArray &data)
{
    return QCryptographicHash::hash(data,QCryptographicHash::Md5);
}

QByteArray Util::getRandomByteArray(int length)
{
    QByteArray ret(length,0);
    for(int i=0;i<length;++i)ret[i] = getRandomNumber() % 256;
    return ret;
}

QByteArray Util::cutBytes(const QByteArray &data, int limitLength)
{
    if(limitLength > data.size())abort();//the size is wrong
    return data.mid(0,limitLength);
}

QByteArray Util::getRandomNumString(int length)
{
    QByteArray ret(length,0);
    for(int i=0;i<length;++i)ret[i] = (getRandomNumber() % 10) + '0';
    return ret;
}

QString Util::getUUID()
{
    return QUuid::createUuid().toString().remove("{").remove("}");
}

int Util::getRandomNumber()
{
    qsrand(getUTCTime());
    return qrand();
}

uint Util::getUTCTime()
{
    return QDateTime::currentDateTimeUtc().toTime_t();
}

std::string Util::BytesStringToStd(const QByteArray &hexStr)
{
    return QByteArray::fromHex(hexStr).toStdString();
}

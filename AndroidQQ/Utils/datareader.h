#ifndef DATAREADER_H
#define DATAREADER_H

#include <QByteArray>
#include <QDataStream>
#include <QIODevice>
#include <QSharedPointer>
#include <QDebug>

class DataReader
{
public:
    enum LengthType{
        SHORT = 0,
        INT = 1,
        BYTE = 2
    };
public:
    DataReader() = default;
    ~DataReader();
    DataReader(const QByteArray &data);
    DataReader & setData(const QByteArray &data);
    DataReader & readLong(qint64 & data);
    DataReader & readInt(int & data);
    DataReader & readShort(short & data);
    DataReader & readByte(qint8 & data);
    DataReader & readDouble(double & data);
    DataReader & readFloat(float & data);
    DataReader & readBytes(QByteArray & data,LengthType lengthType=LengthType::INT,int lengthOffset=0);
    // will append to data,not to clear it
    DataReader & readRaw(QByteArray & data,int length);
    DataReader & readAll(QByteArray & data);
    uint getUnReadSize();
    DataReader & skipBytes(int length);
    DataReader & readTLVS(QMap<short,QByteArray> & data);
    //DataReader & readTLVS(QMap<short,QVariant> & data);
private:
    QSharedPointer<QDataStream> ds = nullptr;
};

#endif // DATAREADER_H

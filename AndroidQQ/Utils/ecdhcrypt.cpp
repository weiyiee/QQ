#include "ecdhcrypt.h"
/*
void ECDHCrypt::calcShareKeyByPeerPublicKey(QByteArray publicKey)
{
    QSslConfiguration config;
    QVector<QSslEllipticCurve> params;
    params<<QSslEllipticCurve::fromLongName("secp192k1");
    config.setEllipticCurves(params);

}
*/

QByteArray ECDHCrypt::initSharedKey = QByteArray::fromHex("3046301006072A8648CE3D020106052B8104001F03320004928D8850673088B343264E0C6BACB8496D697799F37211DEB25BB73906CB089FEA9639B4E0260498B51A992D50813DA8");

QByteArray ECDHCrypt::getPublicKey()
{
    return QByteArray::fromHex("04 CB 36 66 98 56 1E 93 6E 80 C1 57 E0 74 CA B1 3B 0B B6 8D DE B2 82 45 48 A1 B1 8D D4 FB 61 22 AF E1 2F E4 8C 52 66 D8 D7 26 9D 76 51 A8 EB 6F E7");
    //drop(23).take(49)
}

QByteArray ECDHCrypt::getShareKey()
{
    return QByteArray::fromHex("26 33 BA EC 86 EB 79 E6 BC E0 20 06 5E A9 56 6C");
}

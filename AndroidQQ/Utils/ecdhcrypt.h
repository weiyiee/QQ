#ifndef ECDHCRYPT_H
#define ECDHCRYPT_H

#include <QObject>
#include <QSslEllipticCurve>
#include <QSslConfiguration>
#include <QSslCipher>
#include <QSslSocket>
#include <QByteArray>
#include <QVector>

class ECDHCrypt : public QObject
{
    Q_OBJECT
public:
    //void calcShareKeyByPeerPublicKey(QByteArray publicKey);
    static QByteArray getPublicKey();
    static QByteArray getShareKey();
signals:
private:
    static QByteArray initSharedKey;
};

#endif // ECDHCRYPT_H

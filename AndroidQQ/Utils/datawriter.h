#ifndef DATAWRITER_H
#define DATAWRITER_H

#include <QByteArray>
#include <QDataStream>
#include <QIODevice>
#include <QSharedPointer>
#include <QDebug>

class DataWriter
{
public:
    enum LengthType{
        SHORT = 0,
        INT = 1,
        BYTE = 2
    };
public:
    //DataWriter(const DataWriter&) = delete;
    //DataWriter& operator=(const DataWriter &) = delete;
    DataWriter() = delete;
    DataWriter(QSharedPointer<QByteArray> output = nullptr);
    DataWriter & writeShort(short data);
    DataWriter & writeUShort(ushort data);
    DataWriter & writeInt(int data);
    DataWriter & writeLong(quint64 data);
    DataWriter & writeByte(char data);
    DataWriter & writeFloat(float data);
    DataWriter & writeDouble(double data);
    //DataWriter & writeString(QString data,bool useIntLength = true);
    DataWriter & writeBytes(QByteArray data,LengthType lengthType = LengthType::INT,int maxLength = -1,uint lengthOffset = 0);
    QSharedPointer<QByteArray> getData();
    DataWriter & writeRaw(const QByteArray & data);
    DataWriter & writeRaw(DataWriter & dataWriter);
    DataWriter & writeProtobufBytes(const uint & id,const QByteArray & data);
    DataWriter & setToEnd();
    DataWriter & clear();
    int getSize();
    ~DataWriter();
signals:
public:
    void debug(int mode = 0);
private:
    QSharedPointer<QByteArray> output = nullptr;
    QSharedPointer<QDataStream> ds = nullptr;
};

#endif // DATAWRITER_H

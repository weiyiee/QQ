﻿#ifndef PACKETFACTORY_H
#define PACKETFACTORY_H

#include "../Utils/datawriter.h"
#include "../QQInterface/qqdeviceconfig.h"
#include "../Utils/ecdhcrypt.h"
#include "../Utils/tea.h"
#include "../login/tlvs.h"
#include "../login/loginsiginfo.h"
#include "../login/onlinestatus.h"
#include "../generated/Oidb0x769.pb.h"
#include "../generated/MsgSvc.pb.h"
#include "../RequestPacket/requestpackets.h"
#include "../RequestPacket/requestpacket.h"
#include "../generated/Vec0xd50.pb.h"
#include "../generated/SyncCookie.pb.h"

class PacketFactory
{
public:
    QByteArray Login(uint uin,QString password,NetworkType networkType);
    QByteArray StatSvcRegister(const LoginSigInfo & loginSig,OnlineStatus onlineStatus);
    QByteArray ConfigPushSvcPushResp(const LoginSigInfo & loginSig,const int & ssoSequenceId);
    QByteArray HeartbeatAlive(const LoginSigInfo & loginSig);
    QByteArray GetTroopListReqV2Simplify(const LoginSigInfo & loginSig);
    QByteArray SendMessageToGroup(const LoginSigInfo & loginSig,qint64 groupCode,const QString & content);
    QByteArray SendMessageToFriend(const LoginSigInfo & loginSig,qint64 targetUin,const QString & content);
    QByteArray GetMessage(const LoginSigInfo & loginSig,qqprotobuf::MsgSvc::SyncFlag syncFlag = qqprotobuf::MsgSvc::SyncFlag::START);
    QByteArray GetFriendList(const LoginSigInfo & loginSig);
    QByteArray TestFavorite(const LoginSigInfo & loginSig);
private:
    inline uint nextSequenceId();
    inline uint nextMessageSequenceId();
    inline uint nextRequestPacketRequestId();
    inline uint nextHighwayDataTransSequenceIdForGroup();
    inline uint nextHighwayDataTransSequenceIdForFriend();
private:
    uint sequenceId = 85600;
    uint requestPacketRequestId = 1921334513;
    uint highwayDataTransSequenceIdForGroup = 87017;
    uint highwayDataTransSequenceIdForFriend = 40717;
    uint messageSequenceId = 22911;
private:
    static void writeOicqRequestPacket(DataWriter *baseData,uint uin,int cmd,const QByteArray & body,char encryptID = 7);
    static void encryptMakeBody(DataWriter *baseData,const QByteArray & body);
    static void writeSsoPacket(DataWriter *baseData,const QString & cmdName,uint sequenceId,const QByteArray & body,const QByteArray & extraData = QByteArray());
    static void buildOutgoingPacket(DataWriter *baseData,uint uin,char bodyType,const QByteArray & body,const QByteArray & key=QByteArray(16,0),const QByteArray & extraData = QByteArray());
    static void buildResponseUniPacket(DataWriter *baseData,const QString & cmdName,const QByteArray & key,int sequenceId,const QByteArray & outgoingPacketSessionId,uint uin,const QByteArray & body,qint8 bodyType = 1,const QByteArray & extraData = QByteArray());
    static void writeUniPacket(DataWriter *baseData,const QString & cmdName,const QByteArray & outgoingPacketSessionId,const QByteArray & body,const QByteArray & extraData = QByteArray());
};

#endif // PACKETFACTORY_H

#ifndef REQUESTPACKET_H
#define REQUESTPACKET_H

#include "../Utils/jcewriter.h"

class RequestDataVersion3 : public JceWriter
{
public:
    RequestDataVersion3():JceWriter(nullptr){}
    JceWriter & serialize() override;
public:
    QMap<QString,QByteArray> map;
};

class RequestPacket : public JceWriter
{
public:
    RequestPacket():JceWriter(nullptr){};
    JceWriter & serialize() override;
    JceWriter & setSBufferName(QString name);
public:
    short iVersion = 3;
    qint8 cPacketType = 0;
    int iMessageType = 0;
    int iRequestId;
    QString sServantName;
    QString sFuncName;
    QByteArray sBuffer;
    int iTimeout;
    QMap<QString,QString> context;
    QMap<QString,QString> status;
private:
    QString sbufferName;
};

#endif // REQUESTPACKET_H

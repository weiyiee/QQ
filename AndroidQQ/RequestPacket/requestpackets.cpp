#include "requestpackets.h"


JceWriter &TroopListReqV2Simplify::serialize()
{
    writeT<qint64>(0,uin);
    writeT<qint8>(1,getMSFMsgFlag);
    writeT<QByteArray>(2,vecCookies);
    writeT<QList<StTroopNumSimplify>>(3,vecGroupInfo);
    writeT<qint8>(4,groupFlagExt);
    writeT<int>(5,shVersion);
    writeT<qint64>(6,dwCompanyId);
    writeT<qint64>(7,versionNum);
    writeT<qint8>(8,getLongGroupName);
    return *this;
}

JceWriter &StTroopNumSimplify::serialize()
{
    writeT<qint64>(0,groupCode);
    writeT<qint64>(1,dwGroupInfoSeq);
    writeT<qint64>(2,dwGroupFlagExt);
    writeT<qint64>(3,dwGroupRankSeq);
    return *this;
}

JceWriter &SvcReqRegister::serialize()
{
    writeT<qint64>(0,lUin);
    writeT<qint64>(1,lBid);
    writeT<qint8>(2,cConnType);
    writeT<QString>(3,sOther);
    writeT<int>(4,iStatus);
    writeT<qint8>(5,bOnlinePush);
    writeT<qint8>(6,bIsOnline);
    writeT<qint8>(7,bIsShowOnline);
    writeT<qint8>(8,bKikPC);
    writeT<qint8>(9,bKikWeak);
    writeT<qint64>(10,timeStamp);
    writeT<qint64>(11,iOSVersion);
    writeT<qint8>(12,cNetType);
    writeT<QString>(13,sBuildVer);
    writeT<qint8>(14,bRegType);
    writeT<QByteArray>(15,vecDevParam);
    writeT<QByteArray>(16,vecGuid);
    writeT<int>(17,iLocaleID);
    writeT<qint8>(18,bSlientPush);
    writeT<QString>(19,strDevName);
    writeT<QString>(20,strDevType);
    writeT<QString>(21,strOSVer);
    writeT<qint8>(22,bOpenPush);
    writeT<qint64>(23,iLargeSeq);
    writeT<qint64>(24,iLastWatchStartTime);
    writeT<qint64>(26,uOldSSOIp);
    writeT<qint64>(27,uNewSSOIp);
    writeT<QString>(28,sChannelNo);
    writeT<qint64>(29,lCpId);
    writeT<QString>(30,strVendorName);
    writeT<QString>(31,strVendorOSName);
    writeT<QString>(32,strIOSIdfa);
    writeT<QByteArray>(33,bytes_0x769_reqbody);
    writeT<qint8>(34,bIsSetStatus);
    writeT<QByteArray>(35,vecServerBuf);
    writeT<qint8>(36,bSetMute);
    return *this;
}

JceWriter &GetFriendListReq::serialize()
{
    writeT<int>(0,reqtype);
    writeT<qint8>(1,ifReflush);
    writeT<qint64>(2,uin);
    writeT<short>(3,startIndex);
    writeT<short>(4,getfriendCount);
    writeT<qint8>(5,groupid);
    writeT<qint8>(6,ifGetGroupInfo);
    writeT<qint8>(7,groupstartIndex);
    writeT<qint8>(8,getgroupCount);
    writeT<qint8>(9,ifGetMSFGroup);
    writeT<qint8>(10,ifShowTermType);
    writeT<qint64>(11,version);
    writeT<QList<qint64>>(12,uinList);
    writeT<int>(13,eAppType);
    writeT<qint8>(14,ifGetDOVId);
    writeT<qint8>(15,ifGetBothFlag);
    writeT<QByteArray>(16,vec0xd50Req);
    writeT<QByteArray>(17,vec0xd6bReq);
    writeT<QList<qint64>>(18,vecSnsTypelist);
    return *this;
}

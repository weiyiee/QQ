#ifndef REQUESTPACKETS_H
#define REQUESTPACKETS_H

#include "../Utils/jcewriter.h"

class StTroopNumSimplify : public JceWriter
{
public:
    StTroopNumSimplify():JceWriter(nullptr){};
    JceWriter & serialize() override;
public:
    qint64 groupCode;
    qint64 dwGroupInfoSeq;
    qint64 dwGroupFlagExt;
    qint64 dwGroupRankSeq;
};

class TroopListReqV2Simplify : public JceWriter
{
public:
    TroopListReqV2Simplify():JceWriter(nullptr){}
    JceWriter & serialize() override;
public:
    qint64 uin;
    qint8 getMSFMsgFlag;
    QByteArray vecCookies;
    QList<StTroopNumSimplify> vecGroupInfo; //null fuck code
    qint8 groupFlagExt;
    int shVersion;
    qint64 dwCompanyId;
    qint64 versionNum;
    qint8 getLongGroupName;
};

class SvcReqRegister : public JceWriter
{
public:
    SvcReqRegister():JceWriter(nullptr){}
    JceWriter & serialize() override;
public:
    qint64 lUin = 0;
    qint64 lBid = 0;
    qint8 cConnType = 0;
    QString sOther;
    int iStatus = 11;
    qint8 bOnlinePush = 0;
    qint8 bIsOnline = 0;
    qint8 bIsShowOnline = 0;
    qint8 bKikPC = 0;
    qint8 bKikWeak = 0;
    qint64 timeStamp = 0;
    qint64 iOSVersion = 0;
    qint8 cNetType = 0;
    QString sBuildVer;
    qint8 bRegType = 0;
    QByteArray vecDevParam;
    QByteArray vecGuid;
    int iLocaleID = 2052;
    qint8 bSlientPush = 0;
    QString strDevName;
    QString strDevType;
    QString strOSVer;
    qint8 bOpenPush = 1;
    qint64 iLargeSeq = 0;
    qint64 iLastWatchStartTime = 0;
    qint64 uOldSSOIp = 0;
    qint64 uNewSSOIp = 0;
    QString sChannelNo;
    qint64 lCpId = 0;
    QString strVendorName;
    QString strVendorOSName;
    QString strIOSIdfa;
    QByteArray bytes_0x769_reqbody;
    qint8 bIsSetStatus = 0;
    QByteArray vecServerBuf;
    qint8 bSetMute = 0;
};

class GetFriendListReq : public JceWriter
{
public:
    GetFriendListReq():JceWriter(nullptr){}
    JceWriter & serialize() override;
public:
    int reqtype = 0;
    qint8 ifReflush = 0;
    qint64 uin = 0;
    short startIndex = 0;
    short getfriendCount = 0;
    qint8 groupid = 0;
    qint8 ifGetGroupInfo = 0;
    qint8 groupstartIndex = 0;
    qint8 getgroupCount = 0;
    qint8 ifGetMSFGroup = 0;
    qint8 ifShowTermType = 0;
    qint64 version = 0;
    QList<qint64> uinList;
    int eAppType = 0;
    qint8 ifGetDOVId = 0;
    qint8 ifGetBothFlag = 0;
    QByteArray vec0xd50Req;
    QByteArray vec0xd6bReq;
    QList<qint64> vecSnsTypelist;
};

/*
TEMPLATE CODE

class RequestPacket : public JceWriter
{
public:
    RequestPacket():JceWriter(nullptr){}
    JceWriter & serialize() override;
};

*/

#endif // REQUESTPACKETS_H

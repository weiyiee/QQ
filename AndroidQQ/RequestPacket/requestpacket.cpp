#include "requestpacket.h"

JceWriter &RequestDataVersion3::serialize()
{
    writeT<QMap<QString,QByteArray>>(0,map);
    return *this;
}

JceWriter & RequestPacket::serialize()
{
    writeT<short>(1,iVersion);
    writeT<qint8>(2,cPacketType);
    writeT<int>(3,iMessageType);
    writeT<int>(4,iRequestId);
    writeT<QString>(5,sServantName);
    writeT<QString>(6,sFuncName);
    RequestDataVersion3 reqData3;
    reqData3.map[sbufferName==""?sFuncName:sbufferName] = QByteArray::fromHex("0A")+sBuffer+QByteArray::fromHex("0B");
    sBuffer = *reqData3.serialize().getData();
    writeT<QByteArray>(7,sBuffer);//WARN SBUFFER NEED SPECIAL
    writeT<int>(8,iTimeout);
    writeT<QMap<QString,QString>>(9,context);
    writeT<QMap<QString,QString>>(10,status);
    return *this;
}

JceWriter &RequestPacket::setSBufferName(QString name)
{
    sbufferName = name;
    return *this;
}

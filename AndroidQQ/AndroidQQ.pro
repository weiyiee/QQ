TEMPLATE = lib
CONFIG += staticlib
TARGET = AndroidQQ
QT -= gui
#protobuf
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        RequestPacket/requestpacket.cpp \
        RequestPacket/requestpackets.cpp \
        androidqqcore.cpp \
        ResponsePacket/cmdpacketrecv.cpp \
        login/tlvencryptsolver.cpp \
        login/tlvsolver.cpp \
        login/tlvsolvers.cpp \
        ResponsePacket/oicqresponse.cpp \
        RequestPacket/packetfactory.cpp \
        ResponsePacket/recvpacket.cpp \
        ResponsePacket/ssoframe.cpp \
        login/tlvencryptpacket.cpp \
        login/tlvpacket.cpp \
        login/tlvs.cpp \
        QQInterface/qqdeviceconfig.cpp \
        Utils/QtPropertySerializer.cpp \
        Utils/datareader.cpp \
        Utils/datawriter.cpp \
        Utils/ecdhcrypt.cpp \
        Utils/jcereader.cpp \
        Utils/jcewriter.cpp \
        Utils/tea.cpp \
        Utils/util.cpp \
        generated/DeviceInformation.pb.cc \
        generated/ImCommon.pb.cc \
        generated/ImMsg.pb.cc \
        generated/ImMsgBody.pb.cc \
        generated/ImMsgHead.pb.cc \
        generated/ImReceipt.pb.cc \
        generated/MsgComm.pb.cc \
        generated/MsgCtrl.pb.cc \
        generated/MsgSvc.pb.cc \
        generated/Oidb0x769.pb.cc \
        generated/PbPushMsg.pb.cc \
        generated/SubMsgType0x1a.pb.cc \
        generated/SubMsgType0xc1.pb.cc \
        generated/SyncCookie.pb.cc \
        generated/Vec0xd50.pb.cc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    RequestPacket/requestpacket.h \
    RequestPacket/requestpackets.h \
    androidqqcore.h \
    ResponsePacket/cmdpacketrecv.h \
    login/loginrep.h \
    login/loginsiginfo.h \
    login/logintype.h \
    login/networktype.h \
    login/onlinestatus.h \
    login/tlvencryptsolver.h \
    login/tlvsolver.h \
    login/tlvsolvers.h \
    ResponsePacket/oicqresponse.h \
    RequestPacket/packetfactory.h \
    ResponsePacket/recvpacket.h \
    ResponsePacket/ssoframe.h \
    login/tlvencryptpacket.h \
    login/tlvpacket.h \
    login/tlvs.h \
    QQInterface/qqdeviceconfig.h \
    Utils/QtPropertySerializer.h \
    Utils/datareader.h \
    Utils/datawriter.h \
    Utils/ecdhcrypt.h \
    Utils/jcereader.h \
    Utils/jcewriter.h \
    Utils/tea.h \
    Utils/util.h \
    generated/DeviceInformation.pb.h \
    generated/ImCommon.pb.h \
    generated/ImMsg.pb.h \
    generated/ImMsgBody.pb.h \
    generated/ImMsgHead.pb.h \
    generated/ImReceipt.pb.h \
    generated/MsgComm.pb.h \
    generated/MsgCtrl.pb.h \
    generated/MsgSvc.pb.h \
    generated/Oidb0x769.pb.h \
    generated/PbPushMsg.pb.h \
    generated/SubMsgType0x1a.pb.h \
    generated/SubMsgType0xc1.pb.h \
    generated/SyncCookie.pb.h \
    generated/Vec0xd50.pb.h

DISTFILES += \
    Proto/DeviceInformation.proto \
    Proto/ImCommon.proto \
    Proto/ImMsg.proto \
    Proto/ImMsgBody.proto \
    Proto/ImMsgHead.proto \
    Proto/ImReceipt.proto \
    Proto/MsgComm.proto \
    Proto/MsgCtrl.proto \
    Proto/MsgSvc.proto \
    Proto/Oidb0x769.proto \
    Proto/PbPushMsg.proto \
    Proto/SubMsgType0x1a.proto \
    Proto/SubMsgType0xc1.proto \
    Proto/SyncCookie.proto \
    Proto/Vec0xd50.proto
#PROTO_FILES+= \

#qtprotobuf_generated(false,$$PWD/generatedd,false)

include(./AndroidQQ.pri)

#DEFINES += QT_NO_WARNING_OUTPUT QT_NO_DEBUG_OUTPUT

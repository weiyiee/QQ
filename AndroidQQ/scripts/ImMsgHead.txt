@Serializable
internal class ImMsgHead : ProtoBuf {
    @Serializable
    internal class C2CHead(
        @SerialId(1) val toUin: Long = 0L,
        @SerialId(2) val fromUin: Long = 0L,
        @SerialId(3) val ccType: Int = 0,
        @SerialId(4) val ccCmd: Int = 0,
        @SerialId(5) val authPicSig: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(6) val authSig: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(7) val authBuf: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(8) val serverTime: Int = 0,
        @SerialId(9) val clientTime: Int = 0,
        @SerialId(10) val rand: Int = 0,
        @SerialId(11) val ingPhoneNumber: String = ""
    ) : ProtoBuf

    @Serializable
    internal class CSHead(
        @SerialId(1) val uin: Long = 0L,
        @SerialId(2) val command: Int = 0,
        @SerialId(3) val seq: Int = 0,
        @SerialId(4) val version: Int = 0,
        @SerialId(5) val retryTimes: Int = 0,
        @SerialId(6) val clientType: Int = 0,
        @SerialId(7) val pubno: Int = 0,
        @SerialId(8) val localid: Int = 0,
        @SerialId(9) val timezone: Int = 0,
        @ProtoType(ProtoNumberType.FIXED) @SerialId(10) val clientIp: Int = 0,
        @SerialId(11) val clientPort: Int = 0,
        @ProtoType(ProtoNumberType.FIXED) @SerialId(12) val connIp: Int = 0,
        @SerialId(13) val connPort: Int = 0,
        @ProtoType(ProtoNumberType.FIXED) @SerialId(14) val interfaceIp: Int = 0,
        @SerialId(15) val interfacePort: Int = 0,
        @ProtoType(ProtoNumberType.FIXED) @SerialId(16) val actualIp: Int = 0,
        @SerialId(17) val flag: Int = 0,
        @ProtoType(ProtoNumberType.FIXED) @SerialId(18) val timestamp: Int = 0,
        @SerialId(19) val subcmd: Int = 0,
        @SerialId(20) val result: Int = 0,
        @SerialId(21) val appId: Int = 0,
        @SerialId(22) val instanceId: Int = 0,
        @SerialId(23) val sessionId: Long = 0L,
        @SerialId(24) val idcId: Int = 0
    ) : ProtoBuf

    @Serializable
    internal class DeltaHead(
        @SerialId(1) val totalLen: Long = 0L,
        @SerialId(2) val offset: Long = 0L,
        @SerialId(3) val ackOffset: Long = 0L,
        @SerialId(4) val cookie: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(5) val ackCookie: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(6) val result: Int = 0,
        @SerialId(7) val flags: Int = 0
    ) : ProtoBuf

    @Serializable
    internal class Head(
        @SerialId(1) val headType: Int = 0,
        @SerialId(2) val msgCsHead: CSHead? = null,
        @SerialId(3) val msgS2cHead: S2CHead? = null,
        @SerialId(4) val msgHttpconnHead: HttpConnHead? = null,
        @SerialId(5) val paintFlag: Int = 0,
        @SerialId(6) val msgLoginSig: LoginSig? = null,
        @SerialId(7) val msgDeltaHead: DeltaHead? = null,
        @SerialId(8) val msgC2cHead: C2CHead? = null,
        @SerialId(9) val msgSconnHead: SConnHead? = null,
        @SerialId(10) val msgInstCtrl: InstCtrl? = null
    ) : ProtoBuf

    @Serializable
    internal class HttpConnHead(
        @SerialId(1) val uin: Long = 0L,
        @SerialId(2) val command: Int = 0,
        @SerialId(3) val subCommand: Int = 0,
        @SerialId(4) val seq: Int = 0,
        @SerialId(5) val version: Int = 0,
        @SerialId(6) val retryTimes: Int = 0,
        @SerialId(7) val clientType: Int = 0,
        @SerialId(8) val pubNo: Int = 0,
        @SerialId(9) val localId: Int = 0,
        @SerialId(10) val timeZone: Int = 0,
        @ProtoType(ProtoNumberType.FIXED) @SerialId(11) val clientIp: Int = 0,
        @SerialId(12) val clientPort: Int = 0,
        @ProtoType(ProtoNumberType.FIXED) @SerialId(13) val qzhttpIp: Int = 0,
        @SerialId(14) val qzhttpPort: Int = 0,
        @ProtoType(ProtoNumberType.FIXED) @SerialId(15) val sppIp: Int = 0,
        @SerialId(16) val sppPort: Int = 0,
        @SerialId(17) val flag: Int = 0,
        @SerialId(18) val key: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(19) val compressType: Int = 0,
        @SerialId(20) val originSize: Int = 0,
        @SerialId(21) val errorCode: Int = 0,
        @SerialId(22) val msgRedirect: RedirectMsg? = null,
        @SerialId(23) val commandId: Int = 0,
        @SerialId(24) val serviceCmdid: Int = 0,
        @SerialId(25) val msgOidbhead: TransOidbHead? = null
    ) : ProtoBuf

    @Serializable
    internal class InstCtrl(
        @SerialId(1) val msgSendToInst: List<InstInfo>? = listOf(),
        @SerialId(2) val msgExcludeInst: List<InstInfo>? = listOf(),
        @SerialId(3) val msgFromInst: InstInfo? = InstInfo()
    ) : ProtoBuf

    @Serializable
    internal class InstInfo(
        @SerialId(1) val apppid: Int = 0,
        @SerialId(2) val instid: Int = 0,
        @SerialId(3) val platform: Int = 0,
        @SerialId(10) val enumDeviceType: Int /* enum */ = 0
    ) : ProtoBuf

    @Serializable
    internal class LoginSig(
        @SerialId(1) val type: Int = 0,
        @SerialId(2) val sig: ByteArray = EMPTY_BYTE_ARRAY
    ) : ProtoBuf

    @Serializable
    internal class RedirectMsg(
        @ProtoType(ProtoNumberType.FIXED) @SerialId(1) val lastRedirectIp: Int = 0,
        @SerialId(2) val lastRedirectPort: Int = 0,
        @ProtoType(ProtoNumberType.FIXED) @SerialId(3) val redirectIp: Int = 0,
        @SerialId(4) val redirectPort: Int = 0,
        @SerialId(5) val redirectCount: Int = 0
    ) : ProtoBuf

    @Serializable
    internal class S2CHead(
        @SerialId(1) val subMsgtype: Int = 0,
        @SerialId(2) val msgType: Int = 0,
        @SerialId(3) val fromUin: Long = 0L,
        @SerialId(4) val msgId: Int = 0,
        @ProtoType(ProtoNumberType.FIXED) @SerialId(5) val relayIp: Int = 0,
        @SerialId(6) val relayPort: Int = 0,
        @SerialId(7) val toUin: Long = 0L
    ) : ProtoBuf

    @Serializable
    internal class SConnHead : ProtoBuf

    @Serializable
    internal class TransOidbHead(
        @SerialId(1) val command: Int = 0,
        @SerialId(2) val serviceType: Int = 0,
        @SerialId(3) val result: Int = 0,
        @SerialId(4) val errorMsg: String = ""
    ) : ProtoBuf
}
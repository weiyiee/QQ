source = '''internal class GetFriendListReq(
    @SerialId(0) val reqtype: Int? = null,
    @SerialId(1) val ifReflush: Byte? = null,
    @SerialId(2) val uin: Long? = null,
    @SerialId(3) val startIndex: Short? = null,
    @SerialId(4) val getfriendCount: Short? = null,
    @SerialId(5) val groupid: Byte? = null,
    @SerialId(6) val ifGetGroupInfo: Byte? = null,
    @SerialId(7) val groupstartIndex: Byte? = null,
    @SerialId(8) val getgroupCount: Byte? = null,
    @SerialId(9) val ifGetMSFGroup: Byte? = null,
    @SerialId(10) val ifShowTermType: Byte? = null,
    @SerialId(11) val version: Long? = null,
    @SerialId(12) val uinList: List<Long>? = null,
    @SerialId(13) val eAppType: Int = 0,
    @SerialId(14) val ifGetDOVId: Byte? = null,
    @SerialId(15) val ifGetBothFlag: Byte? = null,
    @SerialId(16) val vec0xd50Req: ByteArray? = null,
    @SerialId(17) val vec0xd6bReq: ByteArray? = null,
    @SerialId(18) val vecSnsTypelist: List<Long>? = null
) : JceStruct'''

'''
class RequestPacket : public JceWriter
{
public:
    RequestPacket():JceWriter(nullptr){}
    JceWriter & serialize() override;
public:

};
'''


'''
JceWriter &TroopListReqV2Simplify::serialize()
{
    writeT<qint64>(0,uin);
    writeT<qint8>(1,getMSFMsgFlag);
    writeT<QByteArray>(2,vecCookies);
    writeT<QList<StTroopNumSimplify>>(3,vecGroupInfo);
    writeT<qint8>(4,groupFlagExt);
    writeT<int>(5,shVersion);
    writeT<qint64>(6,dwCompanyId);
    writeT<qint64>(7,versionNum);
    writeT<qint8>(8,getLongGroupName);
    return *this;
}
'''

className = ""
values = []
line = source.replace("    ","").split("\n")
for item in line:
    if(item.startswith("internal")):
        className = item.split(" ")[2].replace("(","")
    if(item.startswith("@SerialId")):
        subitem = item.split(" ")
        tmp = ["","",""]
        tmp[0] = subitem[0].replace("@SerialId(","").replace(")","")
        tmp[1] = subitem[2].replace(":","")
        subitem[3] = subitem[3].replace("?","")
        if subitem[3] == "Byte":
            subitem[3] = "qint8"
        tmp[2] = subitem[3].replace("Map","QMap").replace("Int","int").replace("Short","short").replace("Long","qint64").replace("ByteArray","QByteArray").replace("List","QList").replace("String","QString")
        values.append(tmp)

ret = ""
ret += "class "
ret += className
ret += ''' : public JceWriter
{
public:
    '''
ret += className
ret += '''():JceWriter(nullptr){}
    JceWriter & serialize() override;
public:
'''
for item in values:
    ret += "    "
    ret += item[2] + " "
    ret += item[1]
    if item[2].startswith("qint8") or item[2].startswith("qint64") or item[2].startswith("short") or item[2].startswith("int"):
        ret += " = 0"
    ret += ";\n"

ret += "};"

print(ret)

ret2 = ""
ret2 += '''JceWriter &'''+className+'''::serialize()
{
'''
for item in values:
    #    writeT<qint64>(0,uin);
    ret2 += "    "
    ret2 += "writeT<"
    ret2 += item[2]
    ret2 += ">(" + item[0] + "," + item[1] + ");\n"
ret2 += "    return *this;\n"
ret2 += "}"

print(ret2)

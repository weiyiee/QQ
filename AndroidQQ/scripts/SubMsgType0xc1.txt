@Serializable
internal class SubMsgType0xc1 {
    @Serializable
    internal class NotOnlineImage(
        @SerialId(1) val filePath: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(2) val fileLen: Int = 0,
        @SerialId(3) val downloadPath: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(4) val oldVerSendFile: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(5) val imgType: Int = 0,
        @SerialId(6) val previewsImage: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(7) val picMd5: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(8) val picHeight: Int = 0,
        @SerialId(9) val picWidth: Int = 0,
        @SerialId(10) val resId: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(11) val flag: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(12) val downloadUrl: String = "",
        @SerialId(13) val original: Int = 0
    ) : ProtoBuf

    @Serializable
    internal class MsgBody(
        @SerialId(1) val fileKey: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(2) val fromUin: Long = 0L,
        @SerialId(3) val toUin: Long = 0L,
        @SerialId(4) val status: Int = 0,
        @SerialId(5) val ttl: Int = 0,
        @SerialId(6) val type: Int = 0,
        @SerialId(7) val encryptPreheadLength: Int = 0,
        @SerialId(8) val encryptType: Int = 0,
        @SerialId(9) val encryptKey: ByteArray = EMPTY_BYTE_ARRAY,
        @SerialId(10) val readTimes: Int = 0,
        @SerialId(11) val leftTime: Int = 0,
        @SerialId(12) val notOnlineImage: NotOnlineImage? = null
    ) : ProtoBuf
}
#source = ''''''
import io
filename = input()
file = io.open(filename,"r")
source = file.read()
file.close()

importData = []

def processImport():
    ret = ""
    for item in importData:
        ret += "import \"" + item + ".proto\";\n"
    return ret

def parseType(name):
    if name.startswith("List") or name.startswith("MutableList"):
        return "repeated "+parseType(name.replace("List<","").replace("MutableList<","").replace(">",""))
    if name == "Int":
        return "int32"
    if name == "Long":
        return "int64"
    if name == "ByteArray":
        return "bytes"
    if name == "String":
        return "string"
    if name == "Byte":
        return "byte"
    if name == "Boolean":
        return "bool"
    if name == "ong":
        return "int64"
    if name == "Double":
        return "double"
    if name == "Float":
        return "float"
    #return name
    
    if name.find(".") != -1:
        try:
            importData.index(name.split(".")[0])
        except (IndexError,ValueError):
            importData.append(name.split(".")[0])
        
    return name

def output(className,values):
    ret = ""
    ret += "message "+className+" {\n"
    for item in values:
        ret += "    " + item[2]+" "+item[1]+" = "+item[0]+";\n"
    ret += "}"
    return ret

ret = ""
sum = ""
lines = source.replace("    ","").split('\n')
className = ""
values = []#id,valuename,type
for line in lines:
    subItem = line.split(" ")
    if(subItem[0].startswith("@SerialId")):
        subValue = ["","",""]
        subValue[0] = subItem[0].replace("@SerialId(","").replace(")","")#id
        subValue[1] = subItem[2].replace(":","")#valueName
        subValue[2] = parseType(subItem[3].replace("?","").replace(",",""))
        values.append(subValue)
    if(subItem[0].startswith("internal")):
        if(subItem[1].startswith("data")):
            className = subItem[3].replace("(","")
        else:
            className = subItem[2].replace("(","")
        
    if(subItem[0].startswith(")")):
        #output data
        ret = output(className,values)
        sum += ret +"\n"
        print(ret)
        #print("\n")
        className = ""
        values = []
    if(subItem[0].startswith("data")):
        className = subItem[3].replace("(","")

filename = filename.strip(".txt")

sum = '''syntax="proto3";
package qqprotobuf.'''+filename+''';
''' +processImport()+ sum

file = io.open(filename+".proto","w")
file.write(sum)
file.close()

// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: SyncCookie.proto

#include "SyncCookie.pb.h"

#include <algorithm>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
namespace qqprotobuf {
class SyncCookieDefaultTypeInternal {
 public:
  ::PROTOBUF_NAMESPACE_ID::internal::ExplicitlyConstructed<SyncCookie> _instance;
} _SyncCookie_default_instance_;
}  // namespace qqprotobuf
static void InitDefaultsscc_info_SyncCookie_SyncCookie_2eproto() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::qqprotobuf::_SyncCookie_default_instance_;
    new (ptr) ::qqprotobuf::SyncCookie();
    ::PROTOBUF_NAMESPACE_ID::internal::OnShutdownDestroyMessage(ptr);
  }
  ::qqprotobuf::SyncCookie::InitAsDefaultInstance();
}

::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<0> scc_info_SyncCookie_SyncCookie_2eproto =
    {{ATOMIC_VAR_INIT(::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase::kUninitialized), 0, 0, InitDefaultsscc_info_SyncCookie_SyncCookie_2eproto}, {}};

static ::PROTOBUF_NAMESPACE_ID::Metadata file_level_metadata_SyncCookie_2eproto[1];
static constexpr ::PROTOBUF_NAMESPACE_ID::EnumDescriptor const** file_level_enum_descriptors_SyncCookie_2eproto = nullptr;
static constexpr ::PROTOBUF_NAMESPACE_ID::ServiceDescriptor const** file_level_service_descriptors_SyncCookie_2eproto = nullptr;

const ::PROTOBUF_NAMESPACE_ID::uint32 TableStruct_SyncCookie_2eproto::offsets[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  ~0u,  // no _has_bits_
  PROTOBUF_FIELD_OFFSET(::qqprotobuf::SyncCookie, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  PROTOBUF_FIELD_OFFSET(::qqprotobuf::SyncCookie, time1_),
  PROTOBUF_FIELD_OFFSET(::qqprotobuf::SyncCookie, time_),
  PROTOBUF_FIELD_OFFSET(::qqprotobuf::SyncCookie, unknown1_),
  PROTOBUF_FIELD_OFFSET(::qqprotobuf::SyncCookie, unknown2_),
  PROTOBUF_FIELD_OFFSET(::qqprotobuf::SyncCookie, const1_),
  PROTOBUF_FIELD_OFFSET(::qqprotobuf::SyncCookie, const2_),
  PROTOBUF_FIELD_OFFSET(::qqprotobuf::SyncCookie, unknown3_),
  PROTOBUF_FIELD_OFFSET(::qqprotobuf::SyncCookie, lastsynctime_),
  PROTOBUF_FIELD_OFFSET(::qqprotobuf::SyncCookie, unknown4_),
};
static const ::PROTOBUF_NAMESPACE_ID::internal::MigrationSchema schemas[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  { 0, -1, sizeof(::qqprotobuf::SyncCookie)},
};

static ::PROTOBUF_NAMESPACE_ID::Message const * const file_default_instances[] = {
  reinterpret_cast<const ::PROTOBUF_NAMESPACE_ID::Message*>(&::qqprotobuf::_SyncCookie_default_instance_),
};

const char descriptor_table_protodef_SyncCookie_2eproto[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) =
  "\n\020SyncCookie.proto\022\nqqprotobuf\"\247\001\n\nSyncC"
  "ookie\022\r\n\005time1\030\001 \001(\004\022\014\n\004time\030\002 \001(\004\022\020\n\010un"
  "known1\030\003 \001(\004\022\020\n\010unknown2\030\004 \001(\004\022\016\n\006const1"
  "\030\005 \001(\004\022\016\n\006const2\030\013 \001(\004\022\020\n\010unknown3\030\014 \001(\004"
  "\022\024\n\014lastSyncTime\030\r \001(\004\022\020\n\010unknown4\030\016 \001(\004"
  "b\006proto3"
  ;
static const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable*const descriptor_table_SyncCookie_2eproto_deps[1] = {
};
static ::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase*const descriptor_table_SyncCookie_2eproto_sccs[1] = {
  &scc_info_SyncCookie_SyncCookie_2eproto.base,
};
static ::PROTOBUF_NAMESPACE_ID::internal::once_flag descriptor_table_SyncCookie_2eproto_once;
static bool descriptor_table_SyncCookie_2eproto_initialized = false;
const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_SyncCookie_2eproto = {
  &descriptor_table_SyncCookie_2eproto_initialized, descriptor_table_protodef_SyncCookie_2eproto, "SyncCookie.proto", 208,
  &descriptor_table_SyncCookie_2eproto_once, descriptor_table_SyncCookie_2eproto_sccs, descriptor_table_SyncCookie_2eproto_deps, 1, 0,
  schemas, file_default_instances, TableStruct_SyncCookie_2eproto::offsets,
  file_level_metadata_SyncCookie_2eproto, 1, file_level_enum_descriptors_SyncCookie_2eproto, file_level_service_descriptors_SyncCookie_2eproto,
};

// Force running AddDescriptors() at dynamic initialization time.
static bool dynamic_init_dummy_SyncCookie_2eproto = (  ::PROTOBUF_NAMESPACE_ID::internal::AddDescriptors(&descriptor_table_SyncCookie_2eproto), true);
namespace qqprotobuf {

// ===================================================================

void SyncCookie::InitAsDefaultInstance() {
}
class SyncCookie::_Internal {
 public:
};

SyncCookie::SyncCookie()
  : ::PROTOBUF_NAMESPACE_ID::Message(), _internal_metadata_(nullptr) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:qqprotobuf.SyncCookie)
}
SyncCookie::SyncCookie(const SyncCookie& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      _internal_metadata_(nullptr) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::memcpy(&time1_, &from.time1_,
    static_cast<size_t>(reinterpret_cast<char*>(&unknown4_) -
    reinterpret_cast<char*>(&time1_)) + sizeof(unknown4_));
  // @@protoc_insertion_point(copy_constructor:qqprotobuf.SyncCookie)
}

void SyncCookie::SharedCtor() {
  ::memset(&time1_, 0, static_cast<size_t>(
      reinterpret_cast<char*>(&unknown4_) -
      reinterpret_cast<char*>(&time1_)) + sizeof(unknown4_));
}

SyncCookie::~SyncCookie() {
  // @@protoc_insertion_point(destructor:qqprotobuf.SyncCookie)
  SharedDtor();
}

void SyncCookie::SharedDtor() {
}

void SyncCookie::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const SyncCookie& SyncCookie::default_instance() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&::scc_info_SyncCookie_SyncCookie_2eproto.base);
  return *internal_default_instance();
}


void SyncCookie::Clear() {
// @@protoc_insertion_point(message_clear_start:qqprotobuf.SyncCookie)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  ::memset(&time1_, 0, static_cast<size_t>(
      reinterpret_cast<char*>(&unknown4_) -
      reinterpret_cast<char*>(&time1_)) + sizeof(unknown4_));
  _internal_metadata_.Clear();
}

const char* SyncCookie::_InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  while (!ctx->Done(&ptr)) {
    ::PROTOBUF_NAMESPACE_ID::uint32 tag;
    ptr = ::PROTOBUF_NAMESPACE_ID::internal::ReadTag(ptr, &tag);
    CHK_(ptr);
    switch (tag >> 3) {
      // uint64 time1 = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 8)) {
          time1_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // uint64 time = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 16)) {
          time_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // uint64 unknown1 = 3;
      case 3:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 24)) {
          unknown1_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // uint64 unknown2 = 4;
      case 4:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 32)) {
          unknown2_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // uint64 const1 = 5;
      case 5:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 40)) {
          const1_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // uint64 const2 = 11;
      case 11:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 88)) {
          const2_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // uint64 unknown3 = 12;
      case 12:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 96)) {
          unknown3_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // uint64 lastSyncTime = 13;
      case 13:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 104)) {
          lastsynctime_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // uint64 unknown4 = 14;
      case 14:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 112)) {
          unknown4_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      default: {
      handle_unusual:
        if ((tag & 7) == 4 || tag == 0) {
          ctx->SetLastTag(tag);
          goto success;
        }
        ptr = UnknownFieldParse(tag, &_internal_metadata_, ptr, ctx);
        CHK_(ptr != nullptr);
        continue;
      }
    }  // switch
  }  // while
success:
  return ptr;
failure:
  ptr = nullptr;
  goto success;
#undef CHK_
}

::PROTOBUF_NAMESPACE_ID::uint8* SyncCookie::_InternalSerialize(
    ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:qqprotobuf.SyncCookie)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  // uint64 time1 = 1;
  if (this->time1() != 0) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteUInt64ToArray(1, this->_internal_time1(), target);
  }

  // uint64 time = 2;
  if (this->time() != 0) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteUInt64ToArray(2, this->_internal_time(), target);
  }

  // uint64 unknown1 = 3;
  if (this->unknown1() != 0) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteUInt64ToArray(3, this->_internal_unknown1(), target);
  }

  // uint64 unknown2 = 4;
  if (this->unknown2() != 0) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteUInt64ToArray(4, this->_internal_unknown2(), target);
  }

  // uint64 const1 = 5;
  if (this->const1() != 0) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteUInt64ToArray(5, this->_internal_const1(), target);
  }

  // uint64 const2 = 11;
  if (this->const2() != 0) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteUInt64ToArray(11, this->_internal_const2(), target);
  }

  // uint64 unknown3 = 12;
  if (this->unknown3() != 0) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteUInt64ToArray(12, this->_internal_unknown3(), target);
  }

  // uint64 lastSyncTime = 13;
  if (this->lastsynctime() != 0) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteUInt64ToArray(13, this->_internal_lastsynctime(), target);
  }

  // uint64 unknown4 = 14;
  if (this->unknown4() != 0) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteUInt64ToArray(14, this->_internal_unknown4(), target);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields(), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:qqprotobuf.SyncCookie)
  return target;
}

size_t SyncCookie::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:qqprotobuf.SyncCookie)
  size_t total_size = 0;

  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // uint64 time1 = 1;
  if (this->time1() != 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::UInt64Size(
        this->_internal_time1());
  }

  // uint64 time = 2;
  if (this->time() != 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::UInt64Size(
        this->_internal_time());
  }

  // uint64 unknown1 = 3;
  if (this->unknown1() != 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::UInt64Size(
        this->_internal_unknown1());
  }

  // uint64 unknown2 = 4;
  if (this->unknown2() != 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::UInt64Size(
        this->_internal_unknown2());
  }

  // uint64 const1 = 5;
  if (this->const1() != 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::UInt64Size(
        this->_internal_const1());
  }

  // uint64 const2 = 11;
  if (this->const2() != 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::UInt64Size(
        this->_internal_const2());
  }

  // uint64 unknown3 = 12;
  if (this->unknown3() != 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::UInt64Size(
        this->_internal_unknown3());
  }

  // uint64 lastSyncTime = 13;
  if (this->lastsynctime() != 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::UInt64Size(
        this->_internal_lastsynctime());
  }

  // uint64 unknown4 = 14;
  if (this->unknown4() != 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::UInt64Size(
        this->_internal_unknown4());
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    return ::PROTOBUF_NAMESPACE_ID::internal::ComputeUnknownFieldsSize(
        _internal_metadata_, total_size, &_cached_size_);
  }
  int cached_size = ::PROTOBUF_NAMESPACE_ID::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void SyncCookie::MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:qqprotobuf.SyncCookie)
  GOOGLE_DCHECK_NE(&from, this);
  const SyncCookie* source =
      ::PROTOBUF_NAMESPACE_ID::DynamicCastToGenerated<SyncCookie>(
          &from);
  if (source == nullptr) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:qqprotobuf.SyncCookie)
    ::PROTOBUF_NAMESPACE_ID::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:qqprotobuf.SyncCookie)
    MergeFrom(*source);
  }
}

void SyncCookie::MergeFrom(const SyncCookie& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:qqprotobuf.SyncCookie)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  if (from.time1() != 0) {
    _internal_set_time1(from._internal_time1());
  }
  if (from.time() != 0) {
    _internal_set_time(from._internal_time());
  }
  if (from.unknown1() != 0) {
    _internal_set_unknown1(from._internal_unknown1());
  }
  if (from.unknown2() != 0) {
    _internal_set_unknown2(from._internal_unknown2());
  }
  if (from.const1() != 0) {
    _internal_set_const1(from._internal_const1());
  }
  if (from.const2() != 0) {
    _internal_set_const2(from._internal_const2());
  }
  if (from.unknown3() != 0) {
    _internal_set_unknown3(from._internal_unknown3());
  }
  if (from.lastsynctime() != 0) {
    _internal_set_lastsynctime(from._internal_lastsynctime());
  }
  if (from.unknown4() != 0) {
    _internal_set_unknown4(from._internal_unknown4());
  }
}

void SyncCookie::CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:qqprotobuf.SyncCookie)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void SyncCookie::CopyFrom(const SyncCookie& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:qqprotobuf.SyncCookie)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool SyncCookie::IsInitialized() const {
  return true;
}

void SyncCookie::InternalSwap(SyncCookie* other) {
  using std::swap;
  _internal_metadata_.Swap(&other->_internal_metadata_);
  swap(time1_, other->time1_);
  swap(time_, other->time_);
  swap(unknown1_, other->unknown1_);
  swap(unknown2_, other->unknown2_);
  swap(const1_, other->const1_);
  swap(const2_, other->const2_);
  swap(unknown3_, other->unknown3_);
  swap(lastsynctime_, other->lastsynctime_);
  swap(unknown4_, other->unknown4_);
}

::PROTOBUF_NAMESPACE_ID::Metadata SyncCookie::GetMetadata() const {
  return GetMetadataStatic();
}


// @@protoc_insertion_point(namespace_scope)
}  // namespace qqprotobuf
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::qqprotobuf::SyncCookie* Arena::CreateMaybeMessage< ::qqprotobuf::SyncCookie >(Arena* arena) {
  return Arena::CreateInternal< ::qqprotobuf::SyncCookie >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>

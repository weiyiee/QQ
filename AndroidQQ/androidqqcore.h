#ifndef ANDROIDQQCORE_H
#define ANDROIDQQCORE_H

#include <QTcpSocket>
#include <QTimer>
#include <QQmlEngine>
#include "RequestPacket/packetfactory.h"
#include "ResponsePacket/recvpacket.h"

class AndroidQQCore : public QObject
{
    Q_OBJECT
public:
    enum MessageType{
        Login,
        StatSvcReg,
        HeartBeat,
        ForceOffline,
        GetTroopListReqV2,
        ConnectedServer,
        DisConnectedServer,
        SendMsgRep,
        GroupMessgae,
        FriendList,
        FriendMessage
    };
    Q_ENUM(MessageType)
public:
    AndroidQQCore(QObject *parent = nullptr);
    ~AndroidQQCore();
    static void registerClass();
    Q_INVOKABLE void connnectToHost();
    Q_INVOKABLE void login(uint uin, QString password);
    Q_INVOKABLE void getGroupList();
    Q_INVOKABLE void getFriendList();
    Q_INVOKABLE void getMessage();
    Q_INVOKABLE void sendMessageToGroup(qint64 groupCode,QString content);
    Q_INVOKABLE void sendMessageToFriend(qint64 uin,QString content);
signals:
    void message(const QMap<QString, QVariant> & message);
    //it must be define as QMap<QString, QVariant>
private:
    QTcpSocket socket;
    PacketFactory factory;
    LoginSigInfo loginSigInfo;
    QByteArray recvBuffer;
    int recvLength = 0;
    QTimer timeHeart;
    bool haveHeartRsp = true;
private slots:
    void callback_wtlogin_login(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_StatSvc_register(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_ConfigPushSvc_PushReq(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_Heartbeat_Alive(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_MessageSvc_PushForceOffline(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_friendlist_GetTroopListReqV2(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_MessageSvc_PbSendMsg(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_OnlinePush_PbPushGroupMsg(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_MessageSvc_PushNotify(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_friendlist_getFriendGroupList(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_MessageSvc_PbGetMsg(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_OnlinePush_ReqPush(RecvPacket & recvPacket,QVariantMap & ret);
    void callback_ConfigPushSvc_PushDomain(RecvPacket & recvPacket,QVariantMap & ret);
private:
    bool checkState();
    void callbackThis(QString funcName,RecvPacket & recvPacket,QVariantMap & ret);
    void runHeartBeat();
private slots:
    void onConnected();
    void readyRead();
    void disconnected();
};

#endif // ANDROIDQQCORE_H

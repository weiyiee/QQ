#ifndef RECVPACKET_H
#define RECVPACKET_H

#include "../Utils/datareader.h"
#include "../Utils/tea.h"
#include "../ResponsePacket/ssoframe.h"
#include "../ResponsePacket/oicqresponse.h"
#include "../ResponsePacket/cmdpacketrecv.h"
#include <QMetaType>

class RecvPacket : protected DataReader
{
public:
    RecvPacket() = delete;
    ~RecvPacket();
    RecvPacket(const QByteArray & data,const QByteArray & d2Key = QByteArray());
public:
    int packSize;
    int flag1;//0A/0B
    qint8 flag2;//packet type
    qint8 flag3;
    QByteArray uinString;
    SsoFrame *ssoFrame = nullptr;
    OicqResponse *oicqResponse = nullptr;
    CmdPacketRecv *bodyObject = nullptr;
private:
    QByteArray plainBody;
};

#endif // RECVPACKET_H

#ifndef SSOFRAME_H
#define SSOFRAME_H

#include "../Utils/datareader.h"
#include "../Utils/util.h"
#include <zlib.h>

class SsoFrame : private DataReader
{
public:
    SsoFrame() = delete;
    SsoFrame(const QByteArray & data);
    int packetSize;
    int ssoSequenceId;
    int returnCode;
    QByteArray extraData;
    QByteArray commandName;
    QByteArray outgoingPacketSessionId;
    int dataCompressed;
    int bodySize;
    QByteArray body;
};

#endif // SSOFRAME_H

#ifndef OICQRESPONSE_H
#define OICQRESPONSE_H

#include "../Utils/datareader.h"
#include "../Utils/tea.h"
#include "../Utils/ecdhcrypt.h"

class OicqResponse : private DataReader
{
public:
    OicqResponse() = delete;
    OicqResponse(const QByteArray & data);
public:
    qint8 flag;
    short cmdID;
    int uin;
    short encryptionMethod;
    QByteArray plainBody;
};

#endif // OICQRESPONSE_H

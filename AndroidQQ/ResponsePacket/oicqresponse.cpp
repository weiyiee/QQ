#include "oicqresponse.h"

OicqResponse::OicqResponse(const QByteArray &data):DataReader(data)
{
    readByte(flag);
    if(flag != 2)abort();
    skipBytes(2).skipBytes(2);
    //27 + 2 + body.size
    //const, =8001
    readShort(cmdID);
    skipBytes(2); // const, =0x0001
    readInt(uin);
    readShort(encryptionMethod);
    skipBytes(1);// const = 0
    if(encryptionMethod == 0){
        readRaw(plainBody,getUnReadSize()-1);
        TEA::setKey(ECDHCrypt::getShareKey());
        plainBody = TEA::decrypt(plainBody);
    }else abort();//unimplement
}

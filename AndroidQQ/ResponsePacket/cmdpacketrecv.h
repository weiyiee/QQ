#ifndef CMDPACKETRECV_H
#define CMDPACKETRECV_H

#include <QMetaType>
#include "../Utils/datareader.h"
#include "../login/loginrep.h"
#include "../login/tlvsolvers.h"
#include "../Utils/jcereader.h"
#include "../generated/MsgSvc.pb.h"
#include "../generated/PbPushMsg.pb.h"

//these classes can resolve the data we decrypt from the server.
//then they will put these data to it's own public value.

void RegisterCmdPackClass();

QVariant unpackUniResp(const QByteArray & data,QString key = "",bool special = false);

class CmdPacketRecv : protected DataReader
{
public:
    CmdPacketRecv() = default;
    CmdPacketRecv & setData(const QByteArray & data);
    virtual void unpack() = 0;
};

class wtlogin_login : protected CmdPacketRecv
{
public:
    wtlogin_login() = default;
    void unpack() override;
public:
    qint8 type;
    QMap<short,QByteArray> tlvs;
    LoginResponse loginResponse;
    TlvSolver0x161 tlvSolver0x161;
    TlvSolver0x119 tlvSolver0x119;
    QString deviceLockUrl;
};
Q_DECLARE_METATYPE(wtlogin_login)

class StatSvc_register : protected CmdPacketRecv
{
public:
    StatSvc_register() = default;
    void unpack() override;
public:
};
Q_DECLARE_METATYPE(StatSvc_register)

class friendlist_GetTroopListReqV2 : protected CmdPacketRecv
{
public:
    friendlist_GetTroopListReqV2() = default;
    void unpack() override;
public:
    QVariant groupData;
};
Q_DECLARE_METATYPE(friendlist_GetTroopListReqV2)

class MessageSvc_PbSendMsg : protected CmdPacketRecv
{
public:
    MessageSvc_PbSendMsg() = default;
    void unpack() override;
public:
    int result;
};
Q_DECLARE_METATYPE(MessageSvc_PbSendMsg)


class OnlinePush_PbPushGroupMsg : protected CmdPacketRecv
{
public:
    OnlinePush_PbPushGroupMsg() = default;
    void unpack() override;
public:
    qqprotobuf::OnlinePush::PbPushMsg pbPushMsg;
};
Q_DECLARE_METATYPE(OnlinePush_PbPushGroupMsg)

class OnlinePush_ReqPush : protected CmdPacketRecv
{
public:
    OnlinePush_ReqPush() = default;
    void unpack() override;
public:
};
Q_DECLARE_METATYPE(OnlinePush_ReqPush)

class MessageSvc_PushNotify : protected CmdPacketRecv
{
public:
    MessageSvc_PushNotify() = default;
    void unpack() override;
public:
};
Q_DECLARE_METATYPE(MessageSvc_PushNotify)

class friendlist_getFriendGroupList : protected CmdPacketRecv
{
public:
    friendlist_getFriendGroupList() = default;
    void unpack() override;
public:
    QVariant friendList;
};
Q_DECLARE_METATYPE(friendlist_getFriendGroupList)


class MessageSvc_PbGetMsg : protected CmdPacketRecv
{
public:
    MessageSvc_PbGetMsg() = default;
    void unpack() override;
public:
    qqprotobuf::MsgSvc::PbGetMsgResp resp;
};
Q_DECLARE_METATYPE(MessageSvc_PbGetMsg)

class MessageSvc_PushForceOffline : protected CmdPacketRecv
{
public:
    MessageSvc_PushForceOffline() = default;
    void unpack() override;
public:
};
Q_DECLARE_METATYPE(MessageSvc_PushForceOffline)
//StatSvc_SvcReqMSFLoginNotify

#endif // CMDPACKETRECV_H

#include "tlvencryptsolver.h"

TlvSolver &TlvEncryptSolver::setData(const QByteArray &data)
{
    TEA::setKey(getKey());
    DataReader::setData(TEA::decrypt(data));
    return *this;
}

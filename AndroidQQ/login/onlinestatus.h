#ifndef ONLINESTATUS_H
#define ONLINESTATUS_H

enum OnlineStatus{
    ONLINE = 11,
    OFFLINE = 21,
    AWAY = 31,
    INVISIABLE = 41,
    BUSY = 50,
    QME = 60,
    DND = 70,
    RECVOFFLINEMSG = 95,
    UNKNOWN = -1
};

#endif // ONLINESTATUS_H

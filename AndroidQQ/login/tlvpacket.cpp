#include "tlvpacket.h"

TlvPacket::TlvPacket(short TlvID, DataWriter *baseData):DataWriter(nullptr)
{
    this->baseData = baseData;
    this->TlvID = TlvID;
    //writeShort(TlvID);
    //we can't confirm that the user will call pack().release() at once
    //so we make it lazy to write TLVID
}

void TlvPacket::release()
{
    baseData->writeShort(TlvID);
    const QByteArray & tmp = *getData();
    baseData->writeUShort(tmp.size());
    baseData->writeRaw(tmp);
}

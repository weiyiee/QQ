#ifndef LOGINSIGINFO_H
#define LOGINSIGINFO_H

#include <QByteArray>
#include <QString>
#include <QMap>

class LoginSimpleInfo{
public:
    LoginSimpleInfo() = default;
public:
    uint uin;
    short face;
    qint8 age;
    qint8 gender;
    QString nick;
    qint8 imgType;
    qint8 imgFormat;
    QByteArray imgUrl;
    QByteArray mainDisplayName;
};

class C2cMessageSyncData{
public:
    C2cMessageSyncData() = default;
public:
    QByteArray syncCookie;
    QByteArray pubAccountCookie;
    QByteArray msgCtrlBuf;
};

class LoginSigInfo{
public:
    LoginSigInfo() = default;
public:
    uint uin = 0;
    QByteArray encryptA1;
    QByteArray noPicSig;
    QByteArray G;
    QByteArray dpwd;
    QByteArray randSeed;
    LoginSimpleInfo simpleInfo;
    int appPri;
    int a2ExpiryTime;
    int loginBitmap;
    QByteArray tgt;
    uint a2CreationTime;
    QByteArray tgtKey;
    QByteArray sKey;
    QByteArray userSig64;
    QByteArray accessToken;
    QByteArray openId;
    QByteArray openKey;
    QByteArray d2;
    QByteArray d2Key;
    QByteArray sid;
    QByteArray aqSig;
    QMap<QString,QByteArray> psKeyMap;
    QMap<QString,QByteArray> pt4TokenMap;
    QByteArray superKey;
    QByteArray payToken;
    QByteArray pf;
    QByteArray pfKey;
    QByteArray da2;
    QByteArray wtSessionTicket;
    QByteArray wtSessionTicketKey;
    QByteArray deviceToken;
    QByteArray vKey;
    QByteArray userStWebSig;
    QByteArray userStSig;
    QByteArray userStKey;
    QByteArray lsKey;
    QByteArray userA5;
    QByteArray userA8;
    QByteArray outgoingPacketSessionId;
    C2cMessageSyncData c2cMessageSyncData;
};

#endif // LOGINSIGINFO_H

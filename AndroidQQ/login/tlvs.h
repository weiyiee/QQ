#ifndef TLVS_H
#define TLVS_H

#include "tlvpacket.h"
#include "tlvencryptpacket.h"
#include "../QQInterface/qqdeviceconfig.h"
#include "logintype.h"
#include "networktype.h"

//it call the base class functions that will write to the body
//Tlv0x1() = delete; means that we force user to support a basedata

class Tlv0x1 : protected TlvPacket
{
public:
    Tlv0x1() = delete;
    Tlv0x1(DataWriter * baseData):TlvPacket(0x1,baseData){}
    TlvPacket & pack(const uint & uin);
};

class Tlv0x2 : protected TlvPacket
{
public:
    Tlv0x2() = delete;
    Tlv0x2(DataWriter * baseData):TlvPacket(0x2,baseData){}
    TlvPacket & pack(const QString & captchaCode,const QByteArray & captchaToken);
private:
    static short sigVer;
};

class Tlv0x8 : protected TlvPacket
{
public:
    Tlv0x8() = delete;
    Tlv0x8(DataWriter * baseData):TlvPacket(0x8,baseData){}
    TlvPacket & pack();
private:
    static int localId;
};

class Tlv0x18 : protected TlvPacket
{
public:
    Tlv0x18() = delete;
    Tlv0x18(DataWriter * baseData):TlvPacket(0x18,baseData){}
    TlvPacket & pack(const uint & uin);
};

class Tlv0x106 : protected TlvEncryptPacket
{
public:
    Tlv0x106() = delete;
    Tlv0x106(DataWriter * baseData):TlvEncryptPacket(0x106,baseData){}
    TlvPacket & pack(const uint & uin,const QByteArray & password,const LoginType & loginType);
protected:
    QByteArray getKey() override;
private:
    static long salt;
    static bool isGuidAvailable;
    uint uin;
    QByteArray password;
};

class Tlv0x116 : protected TlvPacket
{
public:
    Tlv0x116() = delete;
    Tlv0x116(DataWriter * baseData):TlvPacket(0x116,baseData){}
    TlvPacket & pack();
private:
    static QList<quint64> appIdList;
};

class Tlv0x100 : protected TlvPacket
{
public:
    Tlv0x100() = delete;
    Tlv0x100(DataWriter * baseData):TlvPacket(0x100,baseData){}
    TlvPacket & pack();
};

class Tlv0x107 : protected TlvPacket
{
public:
    Tlv0x107() = delete;
    Tlv0x107(DataWriter * baseData):TlvPacket(0x107,baseData){}
    TlvPacket & pack();
private:
    static int picType;
};

class Tlv0x108 : protected TlvPacket
{
public:
    Tlv0x108() = delete;
    Tlv0x108(DataWriter * baseData):TlvPacket(0x108,baseData){}
    TlvPacket & pack();
};

class Tlv0x104 : protected TlvPacket
{
public:
    Tlv0x104() = delete;
    Tlv0x104(DataWriter * baseData):TlvPacket(0x104,baseData){}
    TlvPacket & pack();//unimplement
};

class Tlv0x174 : protected TlvPacket
{
public:
    Tlv0x174() = delete;
    Tlv0x174(DataWriter * baseData):TlvPacket(0x174,baseData){}
    TlvPacket & pack();
};

class Tlv0x17a : protected TlvPacket
{
public:
    Tlv0x17a() = delete;
    Tlv0x17a(DataWriter * baseData):TlvPacket(0x17a,baseData){}
    TlvPacket & pack();
private:
    static int value;
};

class Tlv0x197 : protected TlvPacket
{
public:
    Tlv0x197() = delete;
    Tlv0x197(DataWriter * baseData):TlvPacket(0x197,baseData){}
    TlvPacket & pack();
private:
    static QByteArray value;
};

class Tlv0x19e : protected TlvPacket
{
public:
    Tlv0x19e() = delete;
    Tlv0x19e(DataWriter * baseData):TlvPacket(0x19e,baseData){}
    TlvPacket & pack();
private:
    static int value;
};

class Tlv0x17c : protected TlvPacket
{
public:
    Tlv0x17c() = delete;
    Tlv0x17c(DataWriter * baseData):TlvPacket(0x17c,baseData){}
    TlvPacket & pack();
};

class Tlv0x401 : protected TlvPacket
{
public:
    Tlv0x401() = delete;
    Tlv0x401(DataWriter * baseData):TlvPacket(0x401,baseData){}
    TlvPacket & pack();
};

class Tlv0x142 : protected TlvPacket
{
public:
    Tlv0x142() = delete;
    Tlv0x142(DataWriter * baseData):TlvPacket(0x142,baseData){}
    TlvPacket & pack();
};

class Tlv0x112 : protected TlvPacket
{
public:
    Tlv0x112() = delete;
    Tlv0x112(DataWriter * baseData):TlvPacket(0x112,baseData){}
    TlvPacket & pack();
};

class Tlv0x144 : protected TlvEncryptPacket
{
public:
    Tlv0x144() = delete;
    Tlv0x144(DataWriter * baseData):TlvEncryptPacket(0x144,baseData){}
    TlvPacket & pack(NetworkType networkType);
protected:
    QByteArray getKey() override;
};

class Tlv0x109 : protected TlvPacket
{
public:
    Tlv0x109() = delete;
    Tlv0x109(DataWriter * baseData):TlvPacket(0x109,baseData){}
    TlvPacket & pack();
};

class Tlv0x52d : protected TlvPacket
{
public:
    Tlv0x52d() = delete;
    Tlv0x52d(DataWriter * baseData):TlvPacket(0x52d,baseData){}
    TlvPacket & pack();
};

class Tlv0x124 : protected TlvPacket
{
public:
    Tlv0x124() = delete;
    Tlv0x124(DataWriter * baseData):TlvPacket(0x124,baseData){}
    TlvPacket & pack(NetworkType networkType);
private:
    static QByteArray unknown;
};

class Tlv0x128 : protected TlvPacket
{
public:
    Tlv0x128() = delete;
    Tlv0x128(DataWriter * baseData):TlvPacket(0x128,baseData){}
    TlvPacket & pack();
};

class Tlv0x16e : protected TlvPacket
{
public:
    Tlv0x16e() = delete;
    Tlv0x16e(DataWriter * baseData):TlvPacket(0x16e,baseData){}
    TlvPacket & pack();
};

class Tlv0x145 : protected TlvPacket
{
public:
    Tlv0x145() = delete;
    Tlv0x145(DataWriter * baseData):TlvPacket(0x145,baseData){}
    TlvPacket & pack();
};

class Tlv0x147 : protected TlvPacket
{
public:
    Tlv0x147() = delete;
    Tlv0x147(DataWriter * baseData):TlvPacket(0x147,baseData){}
    TlvPacket & pack();
};

class Tlv0x166 : protected TlvPacket
{
public:
    Tlv0x166() = delete;
    Tlv0x166(DataWriter * baseData):TlvPacket(0x166,baseData){}
    TlvPacket & pack();
private:
    static int imageType;
};

class Tlv0x16a : protected TlvPacket
{
public:
    Tlv0x16a() = delete;
    Tlv0x16a(DataWriter * baseData):TlvPacket(0x16a,baseData){}
    TlvPacket & pack();
};

class Tlv0x154 : protected TlvPacket
{
public:
    Tlv0x154() = delete;
    Tlv0x154(DataWriter * baseData):TlvPacket(0x154,baseData){}
    TlvPacket & pack(uint ssoSequenceId);
};

class Tlv0x141 : protected TlvPacket
{
public:
    Tlv0x141() = delete;
    Tlv0x141(DataWriter * baseData):TlvPacket(0x141,baseData){}
    TlvPacket & pack(NetworkType networkType);
};

class Tlv0x511 : protected TlvPacket
{
public:
    Tlv0x511() = delete;
    Tlv0x511(DataWriter * baseData):TlvPacket(0x511,baseData){}
    TlvPacket & pack();
};

class Tlv0x172 : protected TlvPacket
{
public:
    Tlv0x172() = delete;
    Tlv0x172(DataWriter * baseData):TlvPacket(0x172,baseData){}
    TlvPacket & pack();
};

class Tlv0x185 : protected TlvPacket
{
public:
    Tlv0x185() = delete;
    Tlv0x185(DataWriter * baseData):TlvPacket(0x185,baseData){}
    TlvPacket & pack();
};

class Tlv0x400 : protected TlvPacket
{
public:
    Tlv0x400() = delete;
    Tlv0x400(DataWriter * baseData):TlvPacket(0x400,baseData){}
    TlvPacket & pack();
};

class Tlv0x187 : protected TlvPacket
{
public:
    Tlv0x187() = delete;
    Tlv0x187(DataWriter * baseData):TlvPacket(0x187,baseData){}
    TlvPacket & pack();
};

class Tlv0x188 : protected TlvPacket
{
public:
    Tlv0x188() = delete;
    Tlv0x188(DataWriter * baseData):TlvPacket(0x188,baseData){}
    TlvPacket & pack();
};

class Tlv0x193 : protected TlvPacket
{
public:
    Tlv0x193() = delete;
    Tlv0x193(DataWriter * baseData):TlvPacket(0x193,baseData){}
    TlvPacket & pack(QString ticket);
};

class Tlv0x194 : protected TlvPacket
{
public:
    Tlv0x194() = delete;
    Tlv0x194(DataWriter * baseData):TlvPacket(0x194,baseData){}
    TlvPacket & pack();
};

class Tlv0x191 : protected TlvPacket
{
public:
    Tlv0x191() = delete;
    Tlv0x191(DataWriter * baseData):TlvPacket(0x191,baseData){}
    TlvPacket & pack();
private:
    static int K;
};

class Tlv0x201 : protected TlvPacket
{
public:
    Tlv0x201() = delete;
    Tlv0x201(DataWriter * baseData):TlvPacket(0x201,baseData){}
    TlvPacket & pack();
};

class Tlv0x202 : protected TlvPacket
{
public:
    Tlv0x202() = delete;
    Tlv0x202(DataWriter * baseData):TlvPacket(0x202,baseData){}
    TlvPacket & pack();
};

class Tlv0x177 : protected TlvPacket
{
public:
    Tlv0x177() = delete;
    Tlv0x177(DataWriter * baseData):TlvPacket(0x177,baseData){}
    TlvPacket & pack();
private:
    static quint64 unknown1;
    static QString unknown2;
};

class Tlv0x516 : protected TlvPacket
{
public:
    Tlv0x516() = delete;
    Tlv0x516(DataWriter * baseData):TlvPacket(0x516,baseData){}
    TlvPacket & pack();
};

class Tlv0x521 : protected TlvPacket
{
public:
    Tlv0x521() = delete;
    Tlv0x521(DataWriter * baseData):TlvPacket(0x521,baseData){}
    TlvPacket & pack();
};

class Tlv0x536 : protected TlvPacket
{
public:
    Tlv0x536() = delete;
    Tlv0x536(DataWriter * baseData):TlvPacket(0x536,baseData){}
    //TlvPacket & pack(const QByteArray & loginExtraData);
     TlvPacket & pack();
};

class Tlv0x525 : protected TlvPacket
{
public:
    Tlv0x525() = delete;
    Tlv0x525(DataWriter * baseData):TlvPacket(0x525,baseData){}
    TlvPacket & pack();
};

#endif // TLVS_H

/*
TEMPLATE CODE

class Tlv : protected TlvPacket
{
public:
    Tlv() = delete;
    Tlv(DataWriter * baseData):TlvPacket(,baseData){}
    TlvPacket & pack();
};

class Tlv : protected TlvEncryptPacket
{
public:
    Tlv() = delete;
    Tlv(DataWriter * baseData):TlvEncryptPacket(,baseData){}
    TlvPacket & pack();
protected:
    QByteArray getKey() override;
};

*/

#ifndef NETWORKTYPE_H
#define NETWORKTYPE_H

enum NetworkType{
    MOBILE = 1,
    WIFI = 2,
    OTHER = 0
};

#endif // NETWORKTYPE_H

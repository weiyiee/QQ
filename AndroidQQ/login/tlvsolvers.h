#ifndef TLVSOLVERS_H
#define TLVSOLVERS_H

#include "loginsiginfo.h"
#include "tlvencryptsolver.h"
#include "../QQInterface/qqdeviceconfig.h"
#include <QMetaType>

void RegisterSolvers();
void RunTlvSolvers(QMap<short,QVariant> & tlvs);

class TlvSolver0x173:public TlvSolver{
public:
    void unpack() override;
public:
    qint8 type;
    QByteArray host;
    short port;
};//just a IPV4 shit packet

class TlvSolver0x17f:public TlvSolver{
public:
    void unpack() override;
public:
    qint8 type;
    QByteArray host;
    short port;
};//just a IPV6 shit packet

class TlvSolver0x161:public TlvSolver{
public:
    void unpack() override;
public:
    QMap<short,QByteArray> tlvs;
    QByteArray rollbackSig;
    TlvSolver0x173 tlvSolver0x173;
    TlvSolver0x17f tlvSolver0x17f;
};

class TlvSolver0x130:public TlvSolver{
public:
    void unpack() override;
public:
    int serverTime;
    QByteArray ipFromT149;
};

class TlvSolver0x113:public TlvSolver{
public:
    void unpack() override;
public:
    int uin;
};

class TlvSolver0x125:public TlvSolver{
public:
    void unpack() override;
public:
    QByteArray openId;
    QByteArray openKey;
};

class TlvSolver0x186:public TlvSolver{
public:
    void unpack() override;
public:
    qint8 pwdFlag;
};

class TlvSolver0x167:public TlvSolver{
public:
    void unpack() override;
public:
    qint8 imgType;
    qint8 imgFormat;
    QByteArray imgUrl;
};

class TlvSolver0x11a:public TlvSolver{
public:
    void unpack() override;
public:
    short face;
    qint8 age;
    qint8 gender;
    QByteArray nick;
};

class TlvSolver0x199:public TlvSolver{
public:
    void unpack() override;
public:
    QByteArray openId;
    QByteArray payToken;
};

class TlvSolver0x200:public TlvSolver{
public:
    void unpack() override;
public:
    QByteArray pf;
    QByteArray pfKey;
};

class TlvSolver0x512:public TlvSolver{
public:
    void unpack() override;
public:
    QMap<QString,QByteArray> pt4token;
    QMap<QString,QByteArray> psKey;
};

class TlvSolver0x531:public TlvSolver{
public:
    void unpack() override;
public:
    QMap<short,QByteArray> tlvs;;
    QByteArray a1;
    QByteArray noPicSig;
};

class TlvSolver0x11f:public TlvSolver{
public:
    void unpack() override;
public:
    int appPri;
};

class TlvSolver0x119:public TlvEncryptSolver{
public:
    void unpack() override;
    QByteArray getKey() override;
public:
    QMap<short,QByteArray> tlvs;
    TlvSolver0x130 tlvSolver0x130;
    TlvSolver0x113 tlvSolver0x113;
    QByteArray mainDisplayName;
    QByteArray ksid;//oh the shit code.it's a single instance class
    TlvSolver0x125 tlvSolver0x125;
    TlvSolver0x186 tlvSolver0x186;
    TlvSolver0x167 tlvSolver0x167;
    QByteArray qrPushSig;
    TlvSolver0x11a tlvSolver0x11a;
    TlvSolver0x199 tlvSolver0x199;
    TlvSolver0x200 tlvSolver0x200;
    TlvSolver0x512 tlvSolver0x512;
    TlvSolver0x531 tlvSolver0x531;
    TlvSolver0x11f tlvSolver0x11f;
    LoginSigInfo loginSigInfo;
};

class TlvSolver0x204:public TlvSolver{
public:
    void unpack() override;
public:
};

/*
TEMPLATE CODE

class TlvSolver:public TlvSolver{
public:
    void unpack() override;
public:
};

class TlvSolver:public TlvEncryptSolver{
public:
    void unpack() override;
    QByteArray getKey() override;
public:
};

*/
#endif // TLVSOLVERS_H

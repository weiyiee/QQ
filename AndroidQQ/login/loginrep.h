#ifndef LOGINREP_H
#define LOGINREP_H

enum LoginResponse{
    LoginSuccess,
    ErrorMessage,
    SolveLoginCaptcha,
    UnsafeDeviceLogin,
    SMSVerifyNeeded
};

#endif // LOGINREP_H

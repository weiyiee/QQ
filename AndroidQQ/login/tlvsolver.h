#ifndef TLVSOLVER_H
#define TLVSOLVER_H

#include <QObject>
#include "../Utils/datareader.h"

class TlvSolver : protected DataReader
{
public:
    TlvSolver() = default;
    virtual TlvSolver & setData(const QByteArray &data);
    virtual void unpack() = 0;
};

#endif // TLVSOLVER_H

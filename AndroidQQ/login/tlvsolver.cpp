#include "tlvsolver.h"

TlvSolver &TlvSolver::setData(const QByteArray &data)
{
    DataReader::setData(data);
    return *this;
}

#ifndef TLVPACKET_H
#define TLVPACKET_H

#include "../Utils/datawriter.h"

//we just use this as baseClass to get a new buffer
class TlvPacket : protected DataWriter
{
public:
    TlvPacket() = delete;
    TlvPacket(short TlvID,DataWriter *baseData);
    //i make datawriter support multi write
    //multi writer can modify one bytearray
    virtual void release();
private:
    short TlvID;
    DataWriter *baseData = nullptr;
};

#endif // TLVPACKET_H

#include "tlvencryptpacket.h"

void TlvEncryptPacket::release()
{
    TEA::setKey(getKey());
    const QByteArray & data = TEA::encrypt(*getData());
    clear();
    writeRaw(data);
    //we must setData() to store our encrypt data
    TlvPacket::release();//it will call getData()
}

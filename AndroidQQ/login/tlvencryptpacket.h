#ifndef TLVENCRYPTPACKET_H
#define TLVENCRYPTPACKET_H

#include "tlvpacket.h"
#include "../Utils/tea.h"

class TlvEncryptPacket : protected TlvPacket
{
public:
    TlvEncryptPacket() = delete;
    TlvEncryptPacket(short TlvID,DataWriter *baseData):TlvPacket(TlvID,baseData){};
    virtual void release();
protected:
    virtual QByteArray getKey() = 0;
    //plain data store in base class
};

#endif // TLVENCRYPTPACKET_H

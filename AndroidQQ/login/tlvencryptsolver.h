#ifndef TLVENCRYPTSOLVER_H
#define TLVENCRYPTSOLVER_H

#include "tlvsolver.h"
#include "../Utils/tea.h"

class TlvEncryptSolver : public TlvSolver
{
public:
    TlvEncryptSolver() = default;
    virtual QByteArray getKey() = 0;
    TlvSolver & setData(const QByteArray &data) override;
};

#endif // TLVENCRYPTSOLVER_H

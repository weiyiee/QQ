// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: test.proto

#include "test.pb.h"

#include <algorithm>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
extern PROTOBUF_INTERNAL_EXPORT_test_2eproto ::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<0> scc_info_Example1_EmbeddedMessage_test_2eproto;
class Example1_EmbeddedMessageDefaultTypeInternal {
 public:
  ::PROTOBUF_NAMESPACE_ID::internal::ExplicitlyConstructed<Example1_EmbeddedMessage> _instance;
} _Example1_EmbeddedMessage_default_instance_;
class Example1DefaultTypeInternal {
 public:
  ::PROTOBUF_NAMESPACE_ID::internal::ExplicitlyConstructed<Example1> _instance;
} _Example1_default_instance_;
static void InitDefaultsscc_info_Example1_test_2eproto() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::_Example1_default_instance_;
    new (ptr) ::Example1();
    ::PROTOBUF_NAMESPACE_ID::internal::OnShutdownDestroyMessage(ptr);
  }
  ::Example1::InitAsDefaultInstance();
}

::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_Example1_test_2eproto =
    {{ATOMIC_VAR_INIT(::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase::kUninitialized), 1, 0, InitDefaultsscc_info_Example1_test_2eproto}, {
      &scc_info_Example1_EmbeddedMessage_test_2eproto.base,}};

static void InitDefaultsscc_info_Example1_EmbeddedMessage_test_2eproto() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::_Example1_EmbeddedMessage_default_instance_;
    new (ptr) ::Example1_EmbeddedMessage();
    ::PROTOBUF_NAMESPACE_ID::internal::OnShutdownDestroyMessage(ptr);
  }
  ::Example1_EmbeddedMessage::InitAsDefaultInstance();
}

::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<0> scc_info_Example1_EmbeddedMessage_test_2eproto =
    {{ATOMIC_VAR_INIT(::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase::kUninitialized), 0, 0, InitDefaultsscc_info_Example1_EmbeddedMessage_test_2eproto}, {}};

static ::PROTOBUF_NAMESPACE_ID::Metadata file_level_metadata_test_2eproto[2];
static constexpr ::PROTOBUF_NAMESPACE_ID::EnumDescriptor const** file_level_enum_descriptors_test_2eproto = nullptr;
static constexpr ::PROTOBUF_NAMESPACE_ID::ServiceDescriptor const** file_level_service_descriptors_test_2eproto = nullptr;

const ::PROTOBUF_NAMESPACE_ID::uint32 TableStruct_test_2eproto::offsets[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  PROTOBUF_FIELD_OFFSET(::Example1_EmbeddedMessage, _has_bits_),
  PROTOBUF_FIELD_OFFSET(::Example1_EmbeddedMessage, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  PROTOBUF_FIELD_OFFSET(::Example1_EmbeddedMessage, int32val_),
  PROTOBUF_FIELD_OFFSET(::Example1_EmbeddedMessage, stringval_),
  1,
  0,
  PROTOBUF_FIELD_OFFSET(::Example1, _has_bits_),
  PROTOBUF_FIELD_OFFSET(::Example1, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  PROTOBUF_FIELD_OFFSET(::Example1, stringval_),
  PROTOBUF_FIELD_OFFSET(::Example1, bytesval_),
  PROTOBUF_FIELD_OFFSET(::Example1, embeddedexample1_),
  PROTOBUF_FIELD_OFFSET(::Example1, repeatedint32val_),
  PROTOBUF_FIELD_OFFSET(::Example1, repeatedstringval_),
  0,
  1,
  2,
  ~0u,
  ~0u,
};
static const ::PROTOBUF_NAMESPACE_ID::internal::MigrationSchema schemas[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  { 0, 7, sizeof(::Example1_EmbeddedMessage)},
  { 9, 19, sizeof(::Example1)},
};

static ::PROTOBUF_NAMESPACE_ID::Message const * const file_default_instances[] = {
  reinterpret_cast<const ::PROTOBUF_NAMESPACE_ID::Message*>(&::_Example1_EmbeddedMessage_default_instance_),
  reinterpret_cast<const ::PROTOBUF_NAMESPACE_ID::Message*>(&::_Example1_default_instance_),
};

const char descriptor_table_protodef_test_2eproto[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) =
  "\n\ntest.proto\"\321\001\n\010Example1\022\021\n\tstringVal\030\001"
  " \001(\t\022\020\n\010bytesVal\030\002 \001(\014\0223\n\020embeddedExampl"
  "e1\030\003 \001(\0132\031.Example1.EmbeddedMessage\022\030\n\020r"
  "epeatedInt32Val\030\004 \003(\005\022\031\n\021repeatedStringV"
  "al\030\005 \003(\t\0326\n\017EmbeddedMessage\022\020\n\010int32Val\030"
  "\001 \002(\005\022\021\n\tstringVal\030\002 \002(\t"
  ;
static const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable*const descriptor_table_test_2eproto_deps[1] = {
};
static ::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase*const descriptor_table_test_2eproto_sccs[2] = {
  &scc_info_Example1_test_2eproto.base,
  &scc_info_Example1_EmbeddedMessage_test_2eproto.base,
};
static ::PROTOBUF_NAMESPACE_ID::internal::once_flag descriptor_table_test_2eproto_once;
static bool descriptor_table_test_2eproto_initialized = false;
const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_test_2eproto = {
  &descriptor_table_test_2eproto_initialized, descriptor_table_protodef_test_2eproto, "test.proto", 224,
  &descriptor_table_test_2eproto_once, descriptor_table_test_2eproto_sccs, descriptor_table_test_2eproto_deps, 2, 0,
  schemas, file_default_instances, TableStruct_test_2eproto::offsets,
  file_level_metadata_test_2eproto, 2, file_level_enum_descriptors_test_2eproto, file_level_service_descriptors_test_2eproto,
};

// Force running AddDescriptors() at dynamic initialization time.
static bool dynamic_init_dummy_test_2eproto = (  ::PROTOBUF_NAMESPACE_ID::internal::AddDescriptors(&descriptor_table_test_2eproto), true);

// ===================================================================

void Example1_EmbeddedMessage::InitAsDefaultInstance() {
}
class Example1_EmbeddedMessage::_Internal {
 public:
  using HasBits = decltype(std::declval<Example1_EmbeddedMessage>()._has_bits_);
  static void set_has_int32val(HasBits* has_bits) {
    (*has_bits)[0] |= 2u;
  }
  static void set_has_stringval(HasBits* has_bits) {
    (*has_bits)[0] |= 1u;
  }
};

Example1_EmbeddedMessage::Example1_EmbeddedMessage()
  : ::PROTOBUF_NAMESPACE_ID::Message(), _internal_metadata_(nullptr) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:Example1.EmbeddedMessage)
}
Example1_EmbeddedMessage::Example1_EmbeddedMessage(const Example1_EmbeddedMessage& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      _internal_metadata_(nullptr),
      _has_bits_(from._has_bits_) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  stringval_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  if (from._internal_has_stringval()) {
    stringval_.AssignWithDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from.stringval_);
  }
  int32val_ = from.int32val_;
  // @@protoc_insertion_point(copy_constructor:Example1.EmbeddedMessage)
}

void Example1_EmbeddedMessage::SharedCtor() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&scc_info_Example1_EmbeddedMessage_test_2eproto.base);
  stringval_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  int32val_ = 0;
}

Example1_EmbeddedMessage::~Example1_EmbeddedMessage() {
  // @@protoc_insertion_point(destructor:Example1.EmbeddedMessage)
  SharedDtor();
}

void Example1_EmbeddedMessage::SharedDtor() {
  stringval_.DestroyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}

void Example1_EmbeddedMessage::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const Example1_EmbeddedMessage& Example1_EmbeddedMessage::default_instance() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&::scc_info_Example1_EmbeddedMessage_test_2eproto.base);
  return *internal_default_instance();
}


void Example1_EmbeddedMessage::Clear() {
// @@protoc_insertion_point(message_clear_start:Example1.EmbeddedMessage)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  if (cached_has_bits & 0x00000001u) {
    stringval_.ClearNonDefaultToEmptyNoArena();
  }
  int32val_ = 0;
  _has_bits_.Clear();
  _internal_metadata_.Clear();
}

const char* Example1_EmbeddedMessage::_InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  _Internal::HasBits has_bits{};
  while (!ctx->Done(&ptr)) {
    ::PROTOBUF_NAMESPACE_ID::uint32 tag;
    ptr = ::PROTOBUF_NAMESPACE_ID::internal::ReadTag(ptr, &tag);
    CHK_(ptr);
    switch (tag >> 3) {
      // required int32 int32Val = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 8)) {
          _Internal::set_has_int32val(&has_bits);
          int32val_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // required string stringVal = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 18)) {
          auto str = _internal_mutable_stringval();
          ptr = ::PROTOBUF_NAMESPACE_ID::internal::InlineGreedyStringParser(str, ptr, ctx);
          #ifndef NDEBUG
          ::PROTOBUF_NAMESPACE_ID::internal::VerifyUTF8(str, "Example1.EmbeddedMessage.stringVal");
          #endif  // !NDEBUG
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      default: {
      handle_unusual:
        if ((tag & 7) == 4 || tag == 0) {
          ctx->SetLastTag(tag);
          goto success;
        }
        ptr = UnknownFieldParse(tag, &_internal_metadata_, ptr, ctx);
        CHK_(ptr != nullptr);
        continue;
      }
    }  // switch
  }  // while
success:
  _has_bits_.Or(has_bits);
  return ptr;
failure:
  ptr = nullptr;
  goto success;
#undef CHK_
}

::PROTOBUF_NAMESPACE_ID::uint8* Example1_EmbeddedMessage::_InternalSerialize(
    ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:Example1.EmbeddedMessage)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  // required int32 int32Val = 1;
  if (cached_has_bits & 0x00000002u) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteInt32ToArray(1, this->_internal_int32val(), target);
  }

  // required string stringVal = 2;
  if (cached_has_bits & 0x00000001u) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::VerifyUTF8StringNamedField(
      this->_internal_stringval().data(), static_cast<int>(this->_internal_stringval().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::SERIALIZE,
      "Example1.EmbeddedMessage.stringVal");
    target = stream->WriteStringMaybeAliased(
        2, this->_internal_stringval(), target);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields(), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:Example1.EmbeddedMessage)
  return target;
}

size_t Example1_EmbeddedMessage::RequiredFieldsByteSizeFallback() const {
// @@protoc_insertion_point(required_fields_byte_size_fallback_start:Example1.EmbeddedMessage)
  size_t total_size = 0;

  if (_internal_has_stringval()) {
    // required string stringVal = 2;
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_stringval());
  }

  if (_internal_has_int32val()) {
    // required int32 int32Val = 1;
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::Int32Size(
        this->_internal_int32val());
  }

  return total_size;
}
size_t Example1_EmbeddedMessage::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:Example1.EmbeddedMessage)
  size_t total_size = 0;

  if (((_has_bits_[0] & 0x00000003) ^ 0x00000003) == 0) {  // All required fields are present.
    // required string stringVal = 2;
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_stringval());

    // required int32 int32Val = 1;
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::Int32Size(
        this->_internal_int32val());

  } else {
    total_size += RequiredFieldsByteSizeFallback();
  }
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    return ::PROTOBUF_NAMESPACE_ID::internal::ComputeUnknownFieldsSize(
        _internal_metadata_, total_size, &_cached_size_);
  }
  int cached_size = ::PROTOBUF_NAMESPACE_ID::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void Example1_EmbeddedMessage::MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:Example1.EmbeddedMessage)
  GOOGLE_DCHECK_NE(&from, this);
  const Example1_EmbeddedMessage* source =
      ::PROTOBUF_NAMESPACE_ID::DynamicCastToGenerated<Example1_EmbeddedMessage>(
          &from);
  if (source == nullptr) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:Example1.EmbeddedMessage)
    ::PROTOBUF_NAMESPACE_ID::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:Example1.EmbeddedMessage)
    MergeFrom(*source);
  }
}

void Example1_EmbeddedMessage::MergeFrom(const Example1_EmbeddedMessage& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:Example1.EmbeddedMessage)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = from._has_bits_[0];
  if (cached_has_bits & 0x00000003u) {
    if (cached_has_bits & 0x00000001u) {
      _has_bits_[0] |= 0x00000001u;
      stringval_.AssignWithDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from.stringval_);
    }
    if (cached_has_bits & 0x00000002u) {
      int32val_ = from.int32val_;
    }
    _has_bits_[0] |= cached_has_bits;
  }
}

void Example1_EmbeddedMessage::CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:Example1.EmbeddedMessage)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void Example1_EmbeddedMessage::CopyFrom(const Example1_EmbeddedMessage& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:Example1.EmbeddedMessage)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool Example1_EmbeddedMessage::IsInitialized() const {
  if ((_has_bits_[0] & 0x00000003) != 0x00000003) return false;
  return true;
}

void Example1_EmbeddedMessage::InternalSwap(Example1_EmbeddedMessage* other) {
  using std::swap;
  _internal_metadata_.Swap(&other->_internal_metadata_);
  swap(_has_bits_[0], other->_has_bits_[0]);
  stringval_.Swap(&other->stringval_, &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
    GetArenaNoVirtual());
  swap(int32val_, other->int32val_);
}

::PROTOBUF_NAMESPACE_ID::Metadata Example1_EmbeddedMessage::GetMetadata() const {
  return GetMetadataStatic();
}


// ===================================================================

void Example1::InitAsDefaultInstance() {
  ::_Example1_default_instance_._instance.get_mutable()->embeddedexample1_ = const_cast< ::Example1_EmbeddedMessage*>(
      ::Example1_EmbeddedMessage::internal_default_instance());
}
class Example1::_Internal {
 public:
  using HasBits = decltype(std::declval<Example1>()._has_bits_);
  static void set_has_stringval(HasBits* has_bits) {
    (*has_bits)[0] |= 1u;
  }
  static void set_has_bytesval(HasBits* has_bits) {
    (*has_bits)[0] |= 2u;
  }
  static const ::Example1_EmbeddedMessage& embeddedexample1(const Example1* msg);
  static void set_has_embeddedexample1(HasBits* has_bits) {
    (*has_bits)[0] |= 4u;
  }
};

const ::Example1_EmbeddedMessage&
Example1::_Internal::embeddedexample1(const Example1* msg) {
  return *msg->embeddedexample1_;
}
Example1::Example1()
  : ::PROTOBUF_NAMESPACE_ID::Message(), _internal_metadata_(nullptr) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:Example1)
}
Example1::Example1(const Example1& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      _internal_metadata_(nullptr),
      _has_bits_(from._has_bits_),
      repeatedint32val_(from.repeatedint32val_),
      repeatedstringval_(from.repeatedstringval_) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  stringval_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  if (from._internal_has_stringval()) {
    stringval_.AssignWithDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from.stringval_);
  }
  bytesval_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  if (from._internal_has_bytesval()) {
    bytesval_.AssignWithDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from.bytesval_);
  }
  if (from._internal_has_embeddedexample1()) {
    embeddedexample1_ = new ::Example1_EmbeddedMessage(*from.embeddedexample1_);
  } else {
    embeddedexample1_ = nullptr;
  }
  // @@protoc_insertion_point(copy_constructor:Example1)
}

void Example1::SharedCtor() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&scc_info_Example1_test_2eproto.base);
  stringval_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  bytesval_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  embeddedexample1_ = nullptr;
}

Example1::~Example1() {
  // @@protoc_insertion_point(destructor:Example1)
  SharedDtor();
}

void Example1::SharedDtor() {
  stringval_.DestroyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  bytesval_.DestroyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  if (this != internal_default_instance()) delete embeddedexample1_;
}

void Example1::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const Example1& Example1::default_instance() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&::scc_info_Example1_test_2eproto.base);
  return *internal_default_instance();
}


void Example1::Clear() {
// @@protoc_insertion_point(message_clear_start:Example1)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  repeatedint32val_.Clear();
  repeatedstringval_.Clear();
  cached_has_bits = _has_bits_[0];
  if (cached_has_bits & 0x00000007u) {
    if (cached_has_bits & 0x00000001u) {
      stringval_.ClearNonDefaultToEmptyNoArena();
    }
    if (cached_has_bits & 0x00000002u) {
      bytesval_.ClearNonDefaultToEmptyNoArena();
    }
    if (cached_has_bits & 0x00000004u) {
      GOOGLE_DCHECK(embeddedexample1_ != nullptr);
      embeddedexample1_->Clear();
    }
  }
  _has_bits_.Clear();
  _internal_metadata_.Clear();
}

const char* Example1::_InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  _Internal::HasBits has_bits{};
  while (!ctx->Done(&ptr)) {
    ::PROTOBUF_NAMESPACE_ID::uint32 tag;
    ptr = ::PROTOBUF_NAMESPACE_ID::internal::ReadTag(ptr, &tag);
    CHK_(ptr);
    switch (tag >> 3) {
      // optional string stringVal = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 10)) {
          auto str = _internal_mutable_stringval();
          ptr = ::PROTOBUF_NAMESPACE_ID::internal::InlineGreedyStringParser(str, ptr, ctx);
          #ifndef NDEBUG
          ::PROTOBUF_NAMESPACE_ID::internal::VerifyUTF8(str, "Example1.stringVal");
          #endif  // !NDEBUG
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // optional bytes bytesVal = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 18)) {
          auto str = _internal_mutable_bytesval();
          ptr = ::PROTOBUF_NAMESPACE_ID::internal::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // optional .Example1.EmbeddedMessage embeddedExample1 = 3;
      case 3:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 26)) {
          ptr = ctx->ParseMessage(_internal_mutable_embeddedexample1(), ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // repeated int32 repeatedInt32Val = 4;
      case 4:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 32)) {
          ptr -= 1;
          do {
            ptr += 1;
            _internal_add_repeatedint32val(::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr));
            CHK_(ptr);
            if (!ctx->DataAvailable(ptr)) break;
          } while (::PROTOBUF_NAMESPACE_ID::internal::ExpectTag<32>(ptr));
        } else if (static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 34) {
          ptr = ::PROTOBUF_NAMESPACE_ID::internal::PackedInt32Parser(_internal_mutable_repeatedint32val(), ptr, ctx);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // repeated string repeatedStringVal = 5;
      case 5:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 42)) {
          ptr -= 1;
          do {
            ptr += 1;
            auto str = _internal_add_repeatedstringval();
            ptr = ::PROTOBUF_NAMESPACE_ID::internal::InlineGreedyStringParser(str, ptr, ctx);
            #ifndef NDEBUG
            ::PROTOBUF_NAMESPACE_ID::internal::VerifyUTF8(str, "Example1.repeatedStringVal");
            #endif  // !NDEBUG
            CHK_(ptr);
            if (!ctx->DataAvailable(ptr)) break;
          } while (::PROTOBUF_NAMESPACE_ID::internal::ExpectTag<42>(ptr));
        } else goto handle_unusual;
        continue;
      default: {
      handle_unusual:
        if ((tag & 7) == 4 || tag == 0) {
          ctx->SetLastTag(tag);
          goto success;
        }
        ptr = UnknownFieldParse(tag, &_internal_metadata_, ptr, ctx);
        CHK_(ptr != nullptr);
        continue;
      }
    }  // switch
  }  // while
success:
  _has_bits_.Or(has_bits);
  return ptr;
failure:
  ptr = nullptr;
  goto success;
#undef CHK_
}

::PROTOBUF_NAMESPACE_ID::uint8* Example1::_InternalSerialize(
    ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:Example1)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  // optional string stringVal = 1;
  if (cached_has_bits & 0x00000001u) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::VerifyUTF8StringNamedField(
      this->_internal_stringval().data(), static_cast<int>(this->_internal_stringval().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::SERIALIZE,
      "Example1.stringVal");
    target = stream->WriteStringMaybeAliased(
        1, this->_internal_stringval(), target);
  }

  // optional bytes bytesVal = 2;
  if (cached_has_bits & 0x00000002u) {
    target = stream->WriteBytesMaybeAliased(
        2, this->_internal_bytesval(), target);
  }

  // optional .Example1.EmbeddedMessage embeddedExample1 = 3;
  if (cached_has_bits & 0x00000004u) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      InternalWriteMessage(
        3, _Internal::embeddedexample1(this), target, stream);
  }

  // repeated int32 repeatedInt32Val = 4;
  for (int i = 0, n = this->_internal_repeatedint32val_size(); i < n; i++) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteInt32ToArray(4, this->_internal_repeatedint32val(i), target);
  }

  // repeated string repeatedStringVal = 5;
  for (int i = 0, n = this->_internal_repeatedstringval_size(); i < n; i++) {
    const auto& s = this->_internal_repeatedstringval(i);
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::VerifyUTF8StringNamedField(
      s.data(), static_cast<int>(s.length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::SERIALIZE,
      "Example1.repeatedStringVal");
    target = stream->WriteString(5, s, target);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields(), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:Example1)
  return target;
}

size_t Example1::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:Example1)
  size_t total_size = 0;

  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // repeated int32 repeatedInt32Val = 4;
  {
    size_t data_size = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      Int32Size(this->repeatedint32val_);
    total_size += 1 *
                  ::PROTOBUF_NAMESPACE_ID::internal::FromIntSize(this->_internal_repeatedint32val_size());
    total_size += data_size;
  }

  // repeated string repeatedStringVal = 5;
  total_size += 1 *
      ::PROTOBUF_NAMESPACE_ID::internal::FromIntSize(repeatedstringval_.size());
  for (int i = 0, n = repeatedstringval_.size(); i < n; i++) {
    total_size += ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
      repeatedstringval_.Get(i));
  }

  cached_has_bits = _has_bits_[0];
  if (cached_has_bits & 0x00000007u) {
    // optional string stringVal = 1;
    if (cached_has_bits & 0x00000001u) {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
          this->_internal_stringval());
    }

    // optional bytes bytesVal = 2;
    if (cached_has_bits & 0x00000002u) {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::BytesSize(
          this->_internal_bytesval());
    }

    // optional .Example1.EmbeddedMessage embeddedExample1 = 3;
    if (cached_has_bits & 0x00000004u) {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(
          *embeddedexample1_);
    }

  }
  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    return ::PROTOBUF_NAMESPACE_ID::internal::ComputeUnknownFieldsSize(
        _internal_metadata_, total_size, &_cached_size_);
  }
  int cached_size = ::PROTOBUF_NAMESPACE_ID::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void Example1::MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:Example1)
  GOOGLE_DCHECK_NE(&from, this);
  const Example1* source =
      ::PROTOBUF_NAMESPACE_ID::DynamicCastToGenerated<Example1>(
          &from);
  if (source == nullptr) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:Example1)
    ::PROTOBUF_NAMESPACE_ID::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:Example1)
    MergeFrom(*source);
  }
}

void Example1::MergeFrom(const Example1& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:Example1)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  repeatedint32val_.MergeFrom(from.repeatedint32val_);
  repeatedstringval_.MergeFrom(from.repeatedstringval_);
  cached_has_bits = from._has_bits_[0];
  if (cached_has_bits & 0x00000007u) {
    if (cached_has_bits & 0x00000001u) {
      _has_bits_[0] |= 0x00000001u;
      stringval_.AssignWithDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from.stringval_);
    }
    if (cached_has_bits & 0x00000002u) {
      _has_bits_[0] |= 0x00000002u;
      bytesval_.AssignWithDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from.bytesval_);
    }
    if (cached_has_bits & 0x00000004u) {
      _internal_mutable_embeddedexample1()->::Example1_EmbeddedMessage::MergeFrom(from._internal_embeddedexample1());
    }
  }
}

void Example1::CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:Example1)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void Example1::CopyFrom(const Example1& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:Example1)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool Example1::IsInitialized() const {
  if (_internal_has_embeddedexample1()) {
    if (!embeddedexample1_->IsInitialized()) return false;
  }
  return true;
}

void Example1::InternalSwap(Example1* other) {
  using std::swap;
  _internal_metadata_.Swap(&other->_internal_metadata_);
  swap(_has_bits_[0], other->_has_bits_[0]);
  repeatedint32val_.InternalSwap(&other->repeatedint32val_);
  repeatedstringval_.InternalSwap(&other->repeatedstringval_);
  stringval_.Swap(&other->stringval_, &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
    GetArenaNoVirtual());
  bytesval_.Swap(&other->bytesval_, &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
    GetArenaNoVirtual());
  swap(embeddedexample1_, other->embeddedexample1_);
}

::PROTOBUF_NAMESPACE_ID::Metadata Example1::GetMetadata() const {
  return GetMetadataStatic();
}


// @@protoc_insertion_point(namespace_scope)
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::Example1_EmbeddedMessage* Arena::CreateMaybeMessage< ::Example1_EmbeddedMessage >(Arena* arena) {
  return Arena::CreateInternal< ::Example1_EmbeddedMessage >(arena);
}
template<> PROTOBUF_NOINLINE ::Example1* Arena::CreateMaybeMessage< ::Example1 >(Arena* arena) {
  return Arena::CreateInternal< ::Example1 >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>

#include <QCoreApplication>
#include "QQRobot/qqrobot.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    qDebug()<<"[启动成功]"<<QDateTime::currentDateTime().toString();
    QTextStream stream(stdin);
    qDebug()<<"请输入帐号：";
    QString uin = stream.readLine();
    qDebug()<<"请输入密码：";
    QString password = stream.readLine();
    QQRobot robot(uin,password,&a);
    return a.exec();
}

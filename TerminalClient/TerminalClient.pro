QT -= gui
TARGET = TerminalClient
CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        QQRobot/qqrobot.cpp \
        main.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

unix:!macx: LIBS += -L$$OUT_PWD/../AndroidQQ/ -lAndroidQQ

INCLUDEPATH += $$PWD/../AndroidQQ
DEPENDPATH += $$PWD/../AndroidQQ

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../AndroidQQ/libAndroidQQ.a

include(../AndroidQQ/AndroidQQ.pri)

HEADERS += \
    QQRobot/qqrobot.h

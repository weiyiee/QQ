#include "qqrobot.h"

QQRobot::QQRobot(QString uin, QString password,QObject * parent):client(parent)
{
    this->uin = uin;
    this->password = password;
    connect(&client,&AndroidQQCore::message,this,&QQRobot::message);
    client.connnectToHost();
}

void QQRobot::message(const QMap<QString, QVariant> &message)
{
    if(!message.contains("type"))return;
    AndroidQQCore::MessageType type = (AndroidQQCore::MessageType)message["type"].toInt();
    switch (type) {
    case AndroidQQCore::MessageType::ConnectedServer:{
        client.login(uin.toInt(),password);
    }break;
    case AndroidQQCore::MessageType::DisConnectedServer:{
        client.connnectToHost();
    }break;
    case AndroidQQCore::MessageType::Login:{
        LoginResponse resp = (LoginResponse)message["LoginResponse"].toInt();
        qDebug()<<"LoginResponse"<<resp;
        if(resp != LoginResponse::LoginSuccess)exit(0);
    }break;
    case AndroidQQCore::MessageType::FriendMessage:{
        auto list = message["content"].toList();
        if(list.count()>0){
            auto msg = list[list.count()-1].toMap();
            if(msg["Uin"].toString() != uin){
                qDebug()<<msg["Uin"].toString()<<msg["message"];
                client.sendMessageToFriend(msg["Uin"].toUInt(),msg["message"].toString());
            }
        }
    }break;
    case AndroidQQCore::MessageType::GroupMessgae:{
        qDebug()<<"GroupMessgae"<<message["groupName"].toString()<<message["groupCode"].toString()<<message["message"].toString();
        if(message["Uin"].toString() != uin)client.sendMessageToGroup(message["groupCode"].toInt(),message["message"].toString());
    }break;
    case AndroidQQCore::MessageType::ForceOffline:{
        qDebug()<<"ForceOffline";
        exit(0);//shut down the app
    }break;
    case AndroidQQCore::MessageType::HeartBeat:{
        qDebug()<<"HeartBeat"<<QDateTime::currentDateTime().toString();
    }break;
    }
}

#ifndef QQROBOT_H
#define QQROBOT_H

#include "../AndroidQQ/androidqqcore.h"

class QQRobot:public QObject
{
    Q_OBJECT
public:
    QQRobot() = delete;
    QQRobot(QString uin,QString password,QObject * parent);
public:
    AndroidQQCore client;
private slots:
    void message(const QMap<QString, QVariant> & message);
private:
    QString uin,password;
};

#endif // QQROBOT_H
